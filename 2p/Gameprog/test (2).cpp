
#include "DxLib.h"
#include "math.h"
#define fai 3.141592
#define dosu fai/180 
#define hs 12			//表示するメニューの数
#define hk 360/hs		//上の角度


int Key[256]; // キーが押さているフレーム数を格納する
		

// キーの入力状態を更新する
int gpUpdateKey(){
	char tmpKey[256]; // 現在のキーの入力状態を格納する
	GetHitKeyStateAll( tmpKey ); // 全てのキーの入力状態を得る
	for( int i=0; i<256; i++ ){ 
		if( tmpKey[i] != 0 ){ // i番のキーコードに対応するキーが押されていたら
			Key[i]++;     // 加算
		} else {              // 押されていなければ
			Key[i] = 0;   // 0にする
		}
	}
	return 0;
}

int WINAPI WinMain(HINSTANCE,HINSTANCE,LPSTR,int){
        ChangeWindowMode(TRUE), DxLib_Init(), SetDrawScreen( DX_SCREEN_BACK ); //ウィンドウモード変更と初期化と裏画面設定

		int menyu[hs];
		int i=0,n=0,m=0,n2=0,m2=0,wait=0,enk=0;
		double  menyukaku=0,mozix=0,moziy=0;
		int haikei = LoadGraph("画像/背景.png");
		int en1 = LoadGraph("画像/en1.png");
		int en2 = LoadGraph("画像/en2.png");

		for(i=0;hs>=i+1;i+=1)
			menyu[i]=i;

		// while(裏画面を表画面に反映, メッセージ処理, 画面クリア, キーの更新)
        while( ScreenFlip()==0 && ProcessMessage()==0 && ClearDrawScreen()==0 && gpUpdateKey()==0 ){
	
		
		if(wait>0)
			wait-=1;
		else if( Key[KEY_INPUT_UP] > 30){
			n2=hk/2;
			wait=hk/2+2;
		}
		else if( Key[KEY_INPUT_DOWN] >30 ){
			m2=hk/2;
			wait=hk/2+2;
		}
		else if( Key[KEY_INPUT_UP] > 0){
			n=hk;
			wait=hk+3;
		}
		else if( Key[KEY_INPUT_DOWN] >0 ){
			m=hk;
			wait=hk+3;
		}
		else if( Key[KEY_INPUT_Z]  > 0){
			menyukaku=0;
		}




		if(n2>0){
			menyukaku+=2;
			n2-=1;
		}
		else if(m2>0){
			menyukaku-=2;
			m2-=1;
		}
		
		else if(n>0){
			menyukaku+=1;
			n-=1;
		}
		else if(m>0){
			menyukaku-=1;
			m-=1;
		}
		
		enk+=1;


		DrawRotaGraph(0,0,4.0,0.0,haikei,TRUE);
		DrawRotaGraph(0,0,4.0,enk*dosu/2,en1,TRUE);
		DrawRotaGraph(370,360,3.0,enk*dosu/2+20,en2,TRUE);
		for(i=0;i<hs;i+=1)
			DrawFormatString( cos((menyukaku+i*hk)* dosu )*200-20,sin((menyukaku+i*hk)* dosu )*200+240,GetColor( 0, 0, 0 ), "%d",menyu[i] );
		DrawFormatString(10,240,255,"%d",n);
		DrawFormatString(30,240,255,"%d",m);
		DrawFormatString(50,240,255,"%d",n2);
		DrawFormatString(70,240,255,"%d",m2);
		DrawFormatString(300,10,255,"%f",menyukaku);
		}
		
	DxLib_End(); // DXライブラリ終了処理
	return 0;
			}