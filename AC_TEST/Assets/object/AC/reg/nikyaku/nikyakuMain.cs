﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.scripts.objct;

public class nikyakuMain : regMain
{
    

    protected override void starts()
    {
        base.starts();
        Jsonpath += "testBooster";

        //この辺はjsonより取得
        id = 0;
        name = "testBooster";
        weight = 10;
        losEN = 10;

        jumpTime = 0.3f;
        notMoveJumpTime = 0.2f;
        boostTime = 0.3f;
        notMoveBoostTime = 0.1f;
        notMoveStopTime = 0.2f;
        
    }

}
