﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.scripts.Systems;

namespace Assets.scripts.objct
{
    //これは自動生成できるようにしたい（ソースコードを）
    public class prtsManager
    {
        private readonly string jsonPath = "Save/saveData";
        [Serializable]
        public class saveDatas
        {
            //unityjsonが配列駄目だったのでbit演算
            public long headPartFlags;
            public long corePartsFlags;
            public long regPartsFlags;
            public long RarmPartsFlags;
            public long LarmPartsFlags;
            public long fcsPartsFlags;
            public long boosterPartsFlags;
            public long genereterPartsFlags;
            public long RweponPartsFlags;
            public long LweponPartsFlags;
            public long RSweponPartsFlags;
            public long LSweponPartsFlags;
        }
        public class ListSaveDatas
        {
            public List<headMain> headPart;
            public List<coreMain> coreParts;
            public List<regMain> regParts;
            public List<armMain> RarmParts;
            public List<armMain> LarmParts;
            public List<fcsMain> fcsParts;
            public List<boosterMain> boosterParts;
            public List<generatorMain> genereterParts;
            public List<weponMain> RweponParts;
            public List<weponMain> LweponParts;
            public List<weponMain> RSweponParts;
            public List<weponMain> LSweponParts;
        }
        public void getParts<T>(long no, out List<T> obj)
            where T : class
        {
            obj = null;
            var Ttype = typeof(T);
            if (Ttype == typeof(headMain)){
                getParts(no, out obj);
            }else if (Ttype == typeof(coreMain)){
                getParts(no, out obj);
            }else if (Ttype == typeof(regMain)){
                getParts(no, out obj);
            }else if (Ttype == typeof(armMain)){
                getParts(no, out obj);
            }else if (Ttype == typeof(fcsMain)){
                getParts(no, out obj);
            }else if (Ttype == typeof(boosterMain))            {
                getParts(no, out obj);
            }else if (Ttype == typeof(generatorMain)){
                getParts(no, out obj);
            }else if (Ttype == typeof(weponMain)){
                getParts(no, out obj);
            }else if (Ttype == typeof(headMain)){
                getParts(no, out obj);
            }
        }

        private void getParts(long no, out List<headMain> head)
        {
            head = new List<headMain>();
            if ((no & 1) != 0)
                head.Add(new headTest());
            if ((no & 2) != 0)
                head.Add(new headTest());
            if ((no & 4) != 0)
                head.Add(new headTest());
            if ((no & 8) != 0)
                head.Add(new headTest());
            if ((no & 16) != 0)
                head.Add(new headTest());
            if ((no & 32) != 0)
                head.Add(new headTest());
        }

        private void getParts(long no, out List<coreMain> core)
        {
            core = new List<coreMain>();
            if ((no & 1) != 0)
                core.Add(new coreTest());
            if ((no & 2) != 0)
                core.Add(new coreTest());
            if ((no & 4) != 0)
                core.Add(new coreTest());
            if ((no & 8) != 0)
                core.Add(new coreTest());
            if ((no & 16) != 0)
                core.Add(new coreTest());
            if ((no & 32) != 0)
                core.Add(new coreTest());

        }

        private void getParts(long no, out List<regMain> reg)
        {
            reg = new List<regMain>();
            if ((no & 1) != 0)
                reg.Add(new regTest());
        }

        private void getParts(long no, out List<armMain> arm)
        {
            arm = new List<armMain>();
            if ((no & 1) != 0)
                arm.Add(new armTest());
        }

        private void getParts(long no, out List<fcsMain> fcs)
        {
            fcs = new List<fcsMain>();
            if ((no & 1) != 0)
                fcs.Add(new fcsTest());
        }

        private void getParts(long no, out List<boosterMain> booster)
        {
            booster = new List<boosterMain>();
            if ((no & 1) != 0)
                booster.Add(new tesstBooster());
        }

        private void getParts(long no, out List<generatorMain> genereter)
        {
            genereter = new List<generatorMain>();
            if ((no & 1) != 0)
                genereter.Add(new GeneretorTest());
        }

        private void getParts(long no, out List<weponMain> wepon)
        {
            wepon = new List<weponMain>();
            if ((no & 1) != 0)
                wepon.Add(new rifleMain());
        }


        public bool setSaveDatas(saveDatas saveDatasClass)
        {
            JsonIO jIO = new JsonIO();
            bool result = jIO.saveJson<saveDatas>(jsonPath, saveDatasClass);

            return result;
        }
        public bool getSaveDatas(out saveDatas saveDataClass)
        {
            JsonIO jIO = new JsonIO();
            bool result = jIO.loadJson<saveDatas>(jsonPath, out saveDataClass);
            return result;
        }

        public ListSaveDatas getAllParts(saveDatas savedata)
        {
            var result = new ListSaveDatas();
            getParts(savedata.headPartFlags,out result.headPart);
            getParts(savedata.headPartFlags, out result.coreParts);
            getParts(savedata.headPartFlags, out result.regParts);
            getParts(savedata.headPartFlags, out result.RarmParts);
            getParts(savedata.headPartFlags, out result.LarmParts);
            getParts(savedata.headPartFlags, out result.fcsParts);
            getParts(savedata.headPartFlags, out result.boosterParts);
            getParts(savedata.headPartFlags, out result.genereterParts);
            getParts(savedata.headPartFlags, out result.RweponParts);
            getParts(savedata.headPartFlags, out result.LweponParts);
            getParts(savedata.headPartFlags, out result.RSweponParts);
            getParts(savedata.headPartFlags, out result.LSweponParts);
            return result;
        }
    }
}
