﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.scripts.objct {
    public class fcsTest : fcsMain
    {

        protected override void starts()
        {
            base.starts();

            //この辺はjsonより取得
            id = 0;
            name = "fcsTest";
            weight = 10;
            losEN = 10;

            size = 0;//非優先ロックサイズ
            speed = 0;
            maxNom = 0;//同時ロック数
            firstSize = 30;//優先ロックサイズ
            distance = 10000;//kyori 

        }
        public override void jsonStart()
        {
            base.jsonStart();
            Jsonpath += "/fcsTest";
            }
    }
}
