﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.scripts.ACsetting
{
    interface IhasGUIContets
    {
        RectTransform getContens();
        void setContents(List<RectTransform> obj);
    }
}
