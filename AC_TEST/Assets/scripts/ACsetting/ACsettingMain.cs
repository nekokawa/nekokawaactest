﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Assets.scripts.ACsetting;
using UnityEngine.UI;
using Assets.UIScript.IhasTwoWayMenue;
using Assets.scripts.objct;

public class ACsettingMain : MonoBehaviour {


    [SerializeField]
    twoWayMenue kindMenu;
    [SerializeField]
    GameObject copyObj;
    [SerializeField]
    Transform MainCanvas;

    private enum ACsettingStreams {start,load,setting,end}
    ACsettingStreams stream_e;
    private bool keyflag;
    private partsKind Pkind;

    private class partMenue
    {
        public twoWayMenue menue;
        public List<ACObjectMain> ACObj;
    }
    private List<partMenue> partList;

    private List<GameObject> _listObj;

    //手順
    //データロード用アニメ再生
    //データロード
    //ロード終了後アニメーション再生
    //GUIをすべてスタートさせる（上を吸収するかも）
    //キーによってアニメーション再生,index変更等々
    //この画面から戻る場合選択されているパーツ構成をセーブ
    //

    // Use this for initialization
    void Start()
    {
        _listObj = new List<GameObject>();
        stream_e = ACsettingStreams.start;
        kindMenu.make();
        kindMenu.Start();
        stream_e = ACsettingStreams.load;
        partList = makeMenu();
        stream_e = ACsettingStreams.setting;
    }
    private List<partMenue> makeMenu()
    {
       var buff = new prtsManager();
        var saveDatas = new prtsManager.saveDatas();
        var partsDatas = new prtsManager.ListSaveDatas();
        buff.getSaveDatas(out saveDatas);
        partsDatas = buff.getAllParts(saveDatas);

        var listObj = new List<GameObject>();
        for(int i=0;i<12;i++)
        {
            _listObj.Add((GameObject)Instantiate(copyObj, MainCanvas, false));
        }
        var result = new List<partMenue>();
        result.Add(makeMenuinMenu(partsDatas.headPart));
        result.Add(makeMenuinMenu(partsDatas.coreParts));
        result.Add(makeMenuinMenu(partsDatas.regParts));
        result.Add(makeMenuinMenu(partsDatas.RarmParts));
        result.Add(makeMenuinMenu(partsDatas.LarmParts));
        result.Add(makeMenuinMenu(partsDatas.fcsParts));
        result.Add(makeMenuinMenu(partsDatas.genereterParts));
        result.Add(makeMenuinMenu(partsDatas.boosterParts));
        result.Add(makeMenuinMenu(partsDatas.RweponParts));
        result.Add(makeMenuinMenu(partsDatas.LweponParts));
        result.Add(makeMenuinMenu(partsDatas.RSweponParts));
        result.Add(makeMenuinMenu(partsDatas.LSweponParts));
        return result;
    }
    private partMenue makeMenuinMenu<T>(List<T> partsDatas)
        where T : ACObjectMain
    {
        var partMenuBuff = new partMenue();
        partMenuBuff.ACObj = partsDatas.Select(x => (ACObjectMain)x).ToList();
        partMenuBuff.menue = Instantiate(copyObj, MainCanvas, false).GetComponent<twoWayMenue>();
        partMenuBuff.menue.makes = partsDatas.Count;
        partMenuBuff.menue.make();
        partMenuBuff.menue.Start();

        return partMenuBuff;
    }
    private IEnumerable<ACObjectMain> AOMchange<T>(List<T>changeObj)
        where T :ACObjectMain
    {
        foreach (ACObjectMain aoj in changeObj)
            yield return aoj;
    }
    // Update is called once per frame
    void Update()
    {
        if (stream_e != ACsettingStreams.setting)
        {
            return;
        }
        //実質コントローラー
        //押されたとき
        var partNow = partList[kindMenu.getIndex()];
        var partsuMenu = partNow.menue;
        if (!(kindMenu.getNotNextFlag() 
            || partsuMenu.getNotNextFlag()))
        {
            if (Input.GetAxisRaw("Vertical") > 0)
            {
                partsuMenu.next();
            }
            else if (Input.GetAxisRaw("Vertical") < 0)
            {
                partsuMenu.prev();
            }
            else if (Input.GetAxisRaw("Horizontal") < 0)
            {
                kindMenu.prev();
            }
            else if (Input.GetAxisRaw("Horizontal") > 0)
            {
                kindMenu.next();
            }
        }
    }
    


}
