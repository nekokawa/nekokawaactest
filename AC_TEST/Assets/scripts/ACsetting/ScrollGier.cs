﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;
using UnityEngine;

namespace Assets.scripts.ACsetting
{
    //回転するタイプの二方向メニュー
    public class ScrollGier : Image
    {

        [SerializeField]
        private float intervalMax;

        private int contentCount;
        private float interval;
        private Transform myTF;


        protected override void Awake()
        {
            base.Awake();
            interval = 0;
            myTF = transform;
            contentCount = myTF.childCount;
        }



        public void Next()
        {
            //子供の全取得について調べること。
            //コントローラーから読み込んでくること
            var tfs =   myTF.GetComponentInChildren<Image>();
        }

        public void Prev()
        {
            myTF.GetChild((contentCount - 1) % contentCount).SetAsFirstSibling();
        }



    }
}
