﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.scripts.objct;
using UnityEngine;
using Assets.scripts.ACMAIN.systems;

namespace Assets.scripts.Systems
{
    class MakeJson : MonoBehaviour
    {
        [SerializeField]
        GameObject test;
        [SerializeField]
        string teststr;
        [ContextMenu("makeJson")]
        public void makeJsonMain()
        {
            var buff = test.GetComponent<fcsMain>();
            buff.jsonStart();
            JsonIO jsonIObuff = new JsonIO();
            jsonIObuff.saveJson<fcsMain>(buff.Jsonpath,buff);
        }

        [ContextMenu("makejsonStr")]
        public void makeJsonSTR()
        {
            var refLoad = new reflectionLoad();
            var buff = new regMain();
            if (refLoad.loadClass<regMain>(teststr, out buff))
            {
                buff.jsonStart();
                JsonIO jsonIObuff = new JsonIO();
                jsonIObuff.saveJson<regMain>(buff.Jsonpath, buff);
            }
            else
            {
                Debug.Log("miss");
            }
        }
        [ContextMenu("testSave")]
        public void makeJsonSaveData()
        {
            var partM = new prtsManager();
            var testDatas = new prtsManager.saveDatas();
            testDatas.boosterPartsFlags = 1;
            testDatas.corePartsFlags = 1;
            testDatas.fcsPartsFlags = 1;
            testDatas.genereterPartsFlags = 1;
            testDatas.headPartFlags = 1;
            testDatas.LarmPartsFlags = 1;
            testDatas.LSweponPartsFlags = 1;
            testDatas.LweponPartsFlags = 1;
            testDatas.RarmPartsFlags = 1;
            testDatas.regPartsFlags = 1;
            testDatas.RSweponPartsFlags = 1;
            testDatas.RweponPartsFlags = 1;

            if (!partM.setSaveDatas(testDatas))
                Debug.Log("Miss");

        }
    }
}
