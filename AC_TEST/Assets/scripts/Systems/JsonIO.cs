﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Assets.scripts.objct;
using UnityEngine;

namespace Assets.scripts.Systems
{


    public class JsonIO
    {
        //constはｄｌｌのなんかで面倒とかなんとか
        readonly private string APPPATH = Application.dataPath + "/";
        public bool saveJson<T>(string path, T saveClass)
        where T:class
        {
            path = APPPATH  + path;
            path += ".txt";
            var result = false;
            var jsonbuff = JsonUtility.ToJson(saveClass);
            //ファイルがなければぁ作るだけぇ
            //if (!File.Exists(path))
            //{
            //    File.CreateText(path);
            //}
            using (var writer = new StreamWriter(path))
            {
                writer.WriteLine(jsonbuff);
                writer.Flush();
                result = true;
            }
            return result;
        }

        public bool loadJson<T>(string path,out T loadClass)
            where T : class
        {
            path = APPPATH + path ;
            path += ".txt";
            var result = false;
            var info = new FileInfo(path);
            using (var reader = new StreamReader(info.OpenRead()))
            {
                var jsonbuff = reader.ReadToEnd();
                loadClass = JsonUtility.FromJson<T>(jsonbuff);
            }
            return result;
        }







        [Serializable]
        public class SaveData
        {
            public string name;
            public int id;
        }
        public class saveDataSec:SaveData
        {
            public string name2;
        }
        public void saveJsonTest<T>(T savedata, string saveName = "mecset")
       where T : class
        {
            //            var data = new SaveData();
            //  var jsonbuff = JsonUtility.ToJson(savedata, true);
            var path = saveName;
            saveJson(path, savedata);
        }

        public SaveData loadJsonTest<T>(string saveName = "mecset")
       where T : SaveData
        {
            T data = null;
            var path = saveName;
            loadJson<T>(path,out data);

            return data;
        }
    }
}
