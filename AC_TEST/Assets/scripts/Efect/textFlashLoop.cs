﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class textFlashLoop : MonoBehaviour {

    [SerializeField]
    float startTime = 0;
    [SerializeField]
    float endTime = 1;

    private float nowTime;
    private int flag=1;
    private CanvasRenderer CRbuff;
	
    void Start()
    {
        nowTime = startTime;
        CRbuff = GetComponent<CanvasRenderer>();
    }

	// Update is called once per frame
	void Update () {
        nowTime = (nowTime + Time.deltaTime * (flag));
        CRbuff.SetAlpha( nowTime / endTime);
        if(nowTime<startTime ||  nowTime>endTime)
        {
            flag *= -1;
        }
	}
}
