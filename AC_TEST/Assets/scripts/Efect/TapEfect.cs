﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapEfect : MonoBehaviour {

    [SerializeField]
    ParticleSystem tapEfect;
    //[SerializeField]
    //Camera _camera;

    private Camera _camera;
    void Start()
    {
        _camera = Camera.main;
    }
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0))
        {
            var pos
                = _camera.ScreenToWorldPoint(Input.mousePosition + _camera.transform.forward * 10);
//            pos.z = pos.z;
            tapEfect.transform.position = pos;
            tapEfect.Emit(1);
        }

	}
}
