﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class circlMake : MonoBehaviour {

    [SerializeField]
    int radios;
    [SerializeField]
    GameObject obj;
    [SerializeField]
    float hankei;

    [ContextMenu("Run")]
    private void run()
    {
        var childs = new List<GameObject>();
        for (int i = 0; i < radios; i++)
        {
            var objbuff = Instantiate(obj);
            objbuff.transform.SetParent(transform);
            float radiosbuff = radios;
            float ibuff = i;
            float fbuff = Mathf.Deg2Rad * ((360.0f/ radiosbuff  * (ibuff-1.0f)));
            objbuff.transform.localPosition = new Vector3( Mathf.Cos(fbuff)*hankei,Mathf.Sin(fbuff)*hankei,0);
            objbuff.transform.localEulerAngles = new Vector3(0,0,360/radios*i);
            objbuff.transform.localScale = Vector3.one;
        }
    }

}
