﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using UnityEngine;

namespace Assets.UIScript.IhasTwoWayMenue
{
    //ポジションの変更と最後に中身の入れ替えでで実現（結構ひどい動きをしている）
    public class LineMenue : twoWayMenue
    {

        //こんなの使わずに均等に並べるのがあった気がする寺シュール参考

        [SerializeField]
        public float makespace;
        //最初はエディタで作れるようにしてたため
        public override void make()
        {
            base.make();
            //いったん中身をすべて削除
            foreach (Transform tf in content.transform)
            {
                var buff = tf.gameObject;
                Destroy(buff);
                buff = null;
            }
            //ugiなら親サイズ変更
            float objsizy = 0f;
            
            for (int i=0; i < makes; i++)
            {
                var objbuff = Instantiate(makeObj);
                objbuff.transform.SetParent(content.transform);
                float ibuff = i;
                var buff = (ibuff * objsizy) + ((ibuff + 1) * makespace);
                objbuff.transform.localPosition = new Vector3(0, buff, 0);
                objbuff.transform.localScale = Vector3.one;
                contentsObj = new List<GameObject>();
                contentsObj.Add(objbuff);
            }
        }

        protected List<RectTransform> childeTF;
        protected float space;
        public override void Start()
        {
            base.Start();
            childeTF = new List<RectTransform>();
            foreach (RectTransform child in content.transform)
            {
                childeTF.Add(child);
            }
            childeTF.Select(tf => tf.position).OrderBy(pos => pos);
            if (childeTF.Count() > 1)
                space = childeTF[1].position.y - childeTF[0].position.y;
            else
                space = 0;

        }
        public override void next()
        {
            base.next();
            StartCoroutine(spinGui(1));
        }
        public override void prev()
        {
            base.prev();
            StartCoroutine(spinGui(-1));
            index = (index-1)%makes;
        }
        public override void nextX(int x)
        {
            base.nextX(x);
            StartCoroutine(spinGui(x));
        }
        /// <summary>
        /// xは進む数 こういうのほどUniRx使うべき
        /// </summary>
        /// <param name="x"></param>
        private IEnumerator spinGui(int x)
        {
            if (makes <= 1)
            {
                yield break;
            }
            notNextFlag = true;

            var ang = Mathf.Deg2Rad * (myTF.rotation.eulerAngles.z+90.0f);
            var speed = (space / makes)*x;
            var lastPos = myTF.localPosition;
            var interval = 0f;
            while (interval < timeMax)
            {
                interval += Time.deltaTime;
                myTF.position +=
                    new Vector3(Mathf.Cos(ang) * (interval / timeMax) * speed
                    , Mathf.Sin(ang)*(interval / timeMax) * speed,0
                    );
                yield return null;
            }
            //子供を一巡
            var nexti = x < 0 ? -1 : 1;
            for (int j = 0; j < Math.Abs(x); j++)
            {
                var buff = childeTF[x < 0 ? makes - 1 : 0].localPosition;
                for (int i = 0; i < makes-1; i++)
                {
                    //マイナスなら逆順に
                    var ibuff = x < 0 ? makes - i-1 : i;
                    childeTF[ibuff].localPosition = childeTF[ibuff + nexti].localPosition;
                }
                childeTF[x < 0 ?0: makes - 1 ].localPosition = buff;
            }
            myTF.localPosition = lastPos;
            notNextFlag = false;
            yield return null;
            //検出が面倒
            //myTF.rotation = Quaternion.Slerp(myTF.rotation,);
        }
    }
}
