﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.UIScript.IhasTwoWayMenue
{
    //親オブジェクトに入れて、子オブジェクトの位置変動
    //オブジェの移動のみを取り扱っている
    //これを継承することでメニューにする予定（インターフェースの方がよかったかも?）
    //現在ボクシングがひでぇことに
    public class twoWayMenue:MonoBehaviour
    {
        [SerializeField]
        public int makes;

        [SerializeField]
        public float timeMax = 0.15f;
        [SerializeField]
        public GameObject makeObj;
        //メニューの中身たちが入ってるおや
        [SerializeField]
        protected GameObject content;
        //特定位置オブジェクト
        [SerializeField]
        private Transform targetObjctTF;

        [SerializeField]
        private IhasTwoWayMenue menuaas;

        protected List<GameObject> contentsObj= new List<GameObject>();
        //現在指示しているあれ
        protected int index;

        protected Transform myTF;
        //indexのチェンジ待ち用
        protected bool notNextFlag;

        /// <summary>
        /// メニュー内容を作成できる(めにゅーには不要な要素かも)
        /// </summary>
        [ContextMenu("Run")]
        virtual public void make(){        }
        /// <summary>
        /// 初期化
        /// </summary>
        virtual public void Start()
        {
            makes = makes==0 ?content.transform.childCount:makes;
            myTF = transform;
            index = makes;
            notNextFlag = false;
        }
        /// <summary>
        /// 一列のメニューは二方向にしか動かない
        /// </summary>
        /// <param name="x"></param>
        virtual public void setX(int x){
            nextX(x- ((index + makes) % makes));
       }virtual public void next(){
            index = (index + 1 + makes) % makes;
       }virtual public void prev(){
            index = (index - 1 + makes) % makes;
        }
        /// <summary>
        /// 一応マイナスにも対応すること
        /// </summary>
        /// <param name="x"></param>
        virtual public void nextX(int x){
            index = (index + x + makes) % makes;
       }public void prevX(int x){
            nextX(-x);
        }
        /// <summary>
        /// いらない気もする
        /// </summary>
        /// <returns></returns>
        public Transform getMyTF()
        {
            return myTF;
        }
        public bool getNotNextFlag()
        {
            return notNextFlag;
        }
        public Transform getTargetTF()
        {
            return AllContentTF().OrderBy(x => (targetObjctTF.position - x.position).magnitude).First();
        }
        public IEnumerable<Transform> AllContentTF()
        {
            foreach(Transform tf in content.transform)
            {
                yield return tf;
            }
        }
        public int getIndex() { return index%makes; }
    }
}
