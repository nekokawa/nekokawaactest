﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.UIScript.IhasTwoWayMenue
{
    interface IhasTwoWayMenue
    {
        void next();
        void prev();
        void nextx(int x);
    }
}
