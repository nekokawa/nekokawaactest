﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using UnityEngine;

namespace Assets.UIScript.IhasTwoWayMenue
{
    //ローテーションを変えることで表現
    //makeCercleをここに入れて、ここで並べた方がいいんじゃ？
    class CiercleMenue: twoWayMenue
    {
        
        public override void Start()
        {
            base.Start();
            
        }
        public override void next()
        {
            base.next();
            StartCoroutine(spinGui(1));
        }
        public override void prev()
        {
            base.prev();
            StartCoroutine(spinGui(-1));
        }
        public override void nextX(int x)
        {
            base.nextX(x);
            StartCoroutine(spinGui(x));
        }
        /// <summary>
        /// xは進む数 こういうのほどUniRx使うべき
        /// </summary>
        /// <param name="x"></param>
        private IEnumerator spinGui(int x)
        {
            notNextFlag = true;
            var nowang = myTF.rotation;
            var ang = (float)x * (360f/(float)myTF.childCount);
            var roteBuff = Quaternion.Euler(0,0,ang);
            var interval = 0f;
            while(interval<timeMax)
            {
                interval += Time.deltaTime;
                myTF.rotation = nowang * Quaternion.Euler(0, 0,  (interval/timeMax)*ang );
                yield return null;
            }
            myTF.rotation = nowang * Quaternion.Euler(0,0,ang);
            notNextFlag = false;
            yield return null;
            //検出が面倒
            //myTF.rotation = Quaternion.Slerp(myTF.rotation,);
        }
    }
}
