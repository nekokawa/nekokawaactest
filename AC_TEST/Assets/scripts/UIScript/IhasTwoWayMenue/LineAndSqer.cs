﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using UnityEngine;

namespace Assets.UIScript.IhasTwoWayMenue
{
    class LineAndSqer :LineMenue    {
        //選択枠の始まりと終わり
        public int startNo;
        public int endNo;
        private int nowNo;

        [SerializeField]
        private Transform targetSqerTF;

        public override void Start()
        {
            base.Start();
            if(startNo!=0)
                base.nextX(-startNo);
            if(childeTF.Count()>1)
            targetSqerTF.position = childeTF[0].position;
        }

        public override void next()
        {
            nowNo += 1;
            if (nowNo > endNo)
            {
                nowNo = endNo;
                base.next();
            }
            else
            {
                StartCoroutine(sqerChage(1));
            }
        }
        public override void prev()
        {
            nowNo -= 1;
            if (nowNo < startNo)
            {
                nowNo = startNo;
                base.prev();
            }
            else
            {
                StartCoroutine(sqerChage(-1));
            }
        }
        public override void nextX(int x)
        {
            nowNo += x;
            if (nowNo > endNo)
            {
                base.nextX(nowNo-endNo);
                nowNo = endNo;
            }
            else if (nowNo < startNo)
            {
                base.nextX(nowNo - startNo);
                nowNo = startNo;
            }
        }
        private IEnumerator sqerChage(int x)
        {
            var lastpos = contentsObj[index + x].transform.position;
            for (float i = 0; i < timeMax; i += Time.deltaTime)
            {
                //麻衣フレーム時間移動
                var movebuff = space * Time.deltaTime * x;
                targetSqerTF.position = new Vector3(0,movebuff,0);
                yield return null;
            }
            targetSqerTF.position = lastpos;
        }
        public void setTargetSqerTF(Transform objbuff)
        {
            targetSqerTF = objbuff;
        }
    }
}
