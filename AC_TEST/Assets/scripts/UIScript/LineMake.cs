﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class LineMake : MonoBehaviour
{
    //こんなの使わずに均等に並べるのがあった気がする寺シュール参考
    [SerializeField]
    public int makes;
    [SerializeField]
    public GameObject obj;
    [SerializeField]
    public float space;

    [ContextMenu("Run")]
    public void run()
    {
        foreach (Transform tf in transform)
        {
            var buff = tf.gameObject;
            Destroy(buff);
            buff = null;
        }
        var childs = new List<GameObject>();
        //ugiなら親サイズ変更
        float objsizy=0f;
        float herfbuff = 0f;
        if (GetComponent(typeof(RectTransform)))
        {
            GetComponent<RectTransform>().sizeDelta
                = new Vector2(
                GetComponent<RectTransform>().sizeDelta.x
                ,(makes+1)*(space+obj.GetComponent<RectTransform>().sizeDelta.y));
            objsizy = obj.GetComponent<RectTransform>().sizeDelta.y;
            herfbuff = GetComponent<RectTransform>().sizeDelta.y/2-(space+objsizy);
        }
        int i = 0;
        //foreach(Transform rTF in transform)
        //{
        //    float ibuff = i;
        //    var buff = (ibuff * objsizy) + ((ibuff + 1) * space) - herfbuff;
        //    rTF.localPosition = new Vector3(0, buff, 0);
        //    rTF.localScale = Vector3.one;
        //    i++;
        //    if (i >= makes)
        //        break;
        //}
        for (; i < makes; i++)
        {
            var objbuff = Instantiate(obj);
            objbuff.transform.SetParent(transform);
            float ibuff = i;
            var buff = (ibuff * objsizy) + ((ibuff + 1) * space) - herfbuff;
            objbuff.transform.localPosition = new Vector3( 0, buff , 0);
            objbuff.transform.localScale = Vector3.one;
        }
        //foreach(Transform tf in EnumTf().OrderBy(x => -x.position.y).Take(transform.childCount - makes))
        //{
        //    DestroyImmediate(tf.gameObject);
        //}
    }
    private IEnumerable<Transform> EnumTf()
    {
        foreach(Transform tf in transform)
        {
            yield return tf;
        }
    }

}
