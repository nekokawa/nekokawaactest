﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.scripts.debug.systems;

namespace Assets.scripts.debug {
    public class fps : MonoBehaviour {


        private SysFpsCounter m_SysCounter = null;
	
    void Awake()
        {
            Application.targetFrameRate = 60;
            DontDestroyOnLoad(this);
            m_SysCounter = new SysFpsCounter();
        }

        // Update is called once per frame
        void Update() {
            m_SysCounter.countUpdate();
    
    }
        void OnGUI()
        {
            string str;
            str = "fps:";
            str += m_SysCounter.GetFps().ToString();

            GUI.Label(new Rect(100, 0, 100, 50), str);
        }
    }
}