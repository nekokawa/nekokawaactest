﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.scripts.debug.systems
{
    class SysFpsCounter
    {
        private float m_intarval = 0.5f;
        private float m_Accum = 0.0f;
        private float m_Rest = 0.0f;
        private float m_Fps = 0.0f;

        private int m_Freame = 0;

        public SysFpsCounter()
        {

        }
        public void countUpdate()
        {
            m_Rest -= Time.deltaTime;
            m_Accum += Time.timeScale / Time.deltaTime;
            m_Freame++;
            if(m_Rest<=0.0f)
            {
                m_Fps = m_Accum / m_Freame;
                m_Rest = m_intarval;
                m_Accum = 0.0f;
                m_Freame = 0;
            }
        }
        public float GetFps()
        {
            return m_Fps;
        }
    }
}
