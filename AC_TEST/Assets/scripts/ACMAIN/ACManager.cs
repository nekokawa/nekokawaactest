﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.scripts.systems;

public class ACManager : MonoBehaviour {

    enum battleFaze {kidou,main,ibent,down,end,result };
    //起動、操作可能時、イベント中、プレイヤーがやられた、ミッション達成等、リザルトへ移行
    


    [SerializeField]
    PlayerMain Pmain;

    [SerializeField]
    enemyManager Emain;

    [SerializeField]
    ammoManeger Amain;



    private battleFaze bFaze;
    // Use this for initialization
    void Start ()
    {

        bFaze = battleFaze.kidou;

	}
	
	// Update is called once per frame
	void Update () {
        switch (bFaze)
        {
            case battleFaze.main: ACmain(); break;
            case battleFaze.ibent: ACibent(); break;
            case battleFaze.result:ACresult(); break;
            case battleFaze.down: ACdown(); break;
            case battleFaze.end: ACend(); break;
            case battleFaze.kidou: ACkidou(); break;
            default: break;
        }
    }
    float timebuff=0;
    void ACkidou()
    {
        bool next=false;
        timebuff += Time.deltaTime;
        if(timebuff>1.2f)
        next = true;
        Pmain.Start();

        if (next)
        {
            Debug.Log("戦闘開始！");
            bFaze = battleFaze.main;
        }
    }
    /// <summary>
    /// 戦闘シーン
    /// </summary>
    void ACmain()
    {
        Pmain.playerUpdate();//playerのアップデート
        Emain.enemyUpdate();//enamyのアップデーと
        Amain.ammoUpdate();//弾の計算

        if (Pmain.endCheck())
            bFaze = battleFaze.down;

    }
    void ACdown()
    {
        if (Input.anyKeyDown)
        {
            Pmain.Start();
            bFaze = battleFaze.main;
        }
    }
    void ACibent()
    {
        bool next = false;
        if (next)
            bFaze = battleFaze.main;

    }
    void ACend()
    {
        bool next = false;
        if (next)
            bFaze = battleFaze.main;

    }
    void ACresult()
    {
        bool next = false;
        if (next)
            bFaze = battleFaze.main;

    }
}
