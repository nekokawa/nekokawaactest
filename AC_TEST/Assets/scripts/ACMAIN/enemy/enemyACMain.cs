﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.scripts.objct;
using Assets.scripts.systems;
using UnityEngine.UI;
using Assets.scripts.ACMAIN.playerControll;
using Assets.ACMAIN.Script.systems;

public class enemyACMain : MonoBehaviour, IObjectManager
{

    //    private List<GameObject> ListGames;
    public Transform myTF;
    public GameObject myGO;
    public Transform Target;
    //    [SerializeField]
    //    playerMove PMove;
    //    [SerializeField]
    //    weponMain Pshot;
    //    [SerializeField]
    //    playerLotate Plotate;

    //    [SerializeField]
    //    armMain Rarm;
    //    [SerializeField]
    //    armMain Larm;

    //    [SerializeField]
    //    fcsMain FCS;

    //    public kitai_t status;
    //    private kitai_t statusOrign;

    //    private playerLock Plock;

    //	// Use this for initialization
    //	void Start ()
    //    {
    //        Plock = new playerLock(Rarm, Larm,FCS);
    //        status.fream.main_ap = 10;
    //        statusOrign = status;
    //	}

    //	// Update is called once per frame
    //	public void playerUpdate () {

    //       // PMove.moveScript();

    //        Plock.lockUpdate();
    //        if (Input.GetButton("Fire1"))
    //        {
    ////            Pshot.weponUpdate();        //本当はハンガーとか考えないといけない
    //        }
    //        Plotate.lotateUpdate();



    //    }

    //    public void IDamager(int atk)
    //    {
    //        status.fream.main_ap -= atk;
    //        APGage.fillAmount = ((float)status.fream.main_ap / (float)statusOrign.fream.main_ap);
    //        if (status.fream.main_ap <= 0)
    //            Debug.Log("END");
    //    }
    //    public void IMover()
    //    {
    //        PMove.moveScript();
    //    }
    //    public void IhasLock(Transform enemyPos)
    //    {

    //    }

    [SerializeField]
    ACMain acCntrol;

    public void enemyStart()
    {
        myTF = transform;
        myGO = gameObject;
        acCntrol.findACDatas();
        acCntrol.gage(APGageChange, ENGageChange);
    }

    public void enemyUpdate()
    {
        acCntrol.acUpdate();

        acCntrol.ACcontrol(Vector3.zero
            , Quaternion.LookRotation(Target.position - myTF.position)
            , false
            , true,true, false, false
            , false,false
            , false, false
            );
    }

    public IEnumerable<Transform> getTransforms()
    {
        yield return myTF;
    }

    public bool endCheck()
    {
        if (acCntrol.hp <= 0)
            return true;
        return false;
    }
    private void APGageChange(int max, int now)
    {
    }
    private void ENGageChange(int max, int now)
    {

    }
}
