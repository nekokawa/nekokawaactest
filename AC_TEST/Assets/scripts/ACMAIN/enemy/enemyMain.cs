﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.scripts.objct;

public class enemyMain : MonoBehaviour,IhasDamager,IhasLock {

    //先にキャッシュじゃないと毎回ゲットコンポーネント
    private Transform playerTF;
    public Transform myTF;

    public GameObject shot;

    private float shotInterval = 0;
    private float shotIntervalMax = 0.1f;

    private kitai_t statas;

    public GameObject ammoManegerOBJ;

    // Use this for initialization
    void Start () {
        myTF = GetComponent<Transform>();
        playerTF = GameObject.Find("playerTarget").GetComponent<Transform>();
        statas.fream.main_ap = 10;

	}
	
	// Update is called once per frame
	public void enemyUpdate ()
    {
        myTF.LookAt(playerTF);
        shotInterval += Time.deltaTime;
        if(shotInterval > shotIntervalMax)
        {
            ammoMain.ammoPara_t buff = new ammoMain.ammoPara_t();
            buff.speed = 50;
            buff.deltime = 2;
            buff.size = new Vector3(1, 1, 1);
            ammoManegerOBJ.GetComponent<ammoManeger>().makeAmmo(transform,transform.rotation, buff, "enemy_ammo");

//            Instantiate(shot, myTF.position, myTF.rotation);
            shotInterval = 0;

        }

    }

    public void IDamager(int atk)
    {
        statas.fream.main_ap -= atk;
        if(statas.fream.main_ap<=0)
        {
            enemyEnd();
        }
    }
    protected void enemyEnd()
    {
        Destroy(gameObject);
    }
    public void IhasLock(Transform enemyPos)
    {

    }

}
