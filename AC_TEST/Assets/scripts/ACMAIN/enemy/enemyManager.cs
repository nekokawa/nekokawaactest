﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.ACMAIN.Script.systems;

public class enemyManager : MonoBehaviour,IObjectManager {

    struct enemyObj
    {
//        public Transform TF;
        public enemyMain Emain;
    }


    private List<enemyACMain> enemys;

	// Use this for initialization
	void Start () {
        //      var buff = new enemyObj();
        //      buff.TF = enemy.GetComponent<Transform>();
        //      buff.Emain = enemy.GetComponent<enemyMain>();
        enemys = new List<enemyACMain>();
        enemys.AddRange(GetComponentsInChildren<enemyACMain>());
        for(int i=0;i<enemys.Count;i++ )
        {
            enemys[i].enemyStart();
        }
//        enemys.Add(enemy);
	}
	
	// Update is called once per frame
	public void enemyUpdate () {
	    for(int i=0; i<enemys.Count;i++)
        {
            if(enemys[i].endCheck())
            {
                //dispose使いましょう←使う意味とは
                Destroy(enemys[i].myGO);
                enemys.RemoveAt(i);
                continue;
            }
            enemys[i].enemyUpdate();
        }
	}


    public IEnumerable<Transform> getTransforms()
    {

        for (int i = 0; i < enemys.Count; i++)
            yield return enemys[i].myTF;
    
            }

}
