﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.scripts.objct
{
    public enum zokusei {k,e};
    public class MainStatus
    {
        public kitai_t status;       
       
        public IEnumerable<string> stateName<T>()
        {
            Type buff = typeof(T);
            foreach (string str in stateNameBace())
                yield return str;
            if (buff == typeof(kitai_t))
            {
                yield break;
            }
            else if(buff==typeof(wepon_t))
            {
                foreach (string str in stateNameAtk())
                    yield return str;
                yield break;
            }
            foreach (string str in stateNameDeff())
                yield return str;
            if (buff == typeof(fream_t))
            {
            }
            else if (buff == typeof(head_t))
            {
            }
            else if (buff == typeof(fream_t))
            {
            }

        }
        private IEnumerable<string> stateNameBace()
        {
            yield return "名前";
            yield return "重量";
        }
        private IEnumerable<string> stateNameDeff()
        {
            yield return "AP";
            yield return "弾道防御";
            yield return "エネルギー防御";
        }
        private IEnumerable<string> stateNameAtk()
        {
            yield return "攻撃力";
            yield return "属性";
            yield return "射程期間";
            yield return "誘導";
            yield return "弾速";
            yield return "球数";
            yield return "弾薬費";
        }

    } 
    public struct kitai_t
    {
        public statasBaceData_t bace;
        public fream_t fream;
        public head_t head;
        public body_t body;
        public reg_t reg;
        public arm_t arm;
        public wepon_t arwep;
        public wepon_t alwep;
        public wepon_t srwep;
        public wepon_t slwep;
    }
    public struct fream_t
    {
        public statasBaceData_t bace;
        public int main_ap;
        public int max_weit;
    }
    public struct head_t
    {
        public statasBaceData_t bace;
        public difenceData_t dData;
    }
    public struct body_t
    {
        public statasBaceData_t bace;
        public difenceData_t dData;
    }
    public struct reg_t
    {
        public statasBaceData_t bace;
        public difenceData_t dData;
    }
    public struct arm_t
    {
        public statasBaceData_t bace;
        public difenceData_t dData;
    }
    public struct wepon_t
    {
        public statasBaceData_t bace;
        public atackData_t aData;
    }

   public struct statasBaceData_t
    {
        public string name;
        public int no;
        public int weit;
    }

    /// <summary>
    /// 防御力基本データ
    /// </summary>
    public struct difenceData_t
    {
        public int ap;
        public int kdef;
        public int edef;
    }
    /// <summary>
    /// 基本攻撃データ
    /// </summary>
    public struct atackData_t
    {
        public int atk;
        public zokusei type;
        public float delTime;
        public int yuudou;
        public int speed;
        public int kazu;
        public int danyakuhi;

    }

}
