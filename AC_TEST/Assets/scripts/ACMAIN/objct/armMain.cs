﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.scripts.ACMAIN.systems;

namespace Assets.scripts.objct
{
    [Serializable]
    public class armMain : freamer
    {
        public int motion;//運動性
        public int stability;//安定性

        private Transform armTF;

        public void setArmTF(Transform armTransform)
        {
            armTF = armTransform;
        }

        protected override void starts()
        {
            //base.starts();//いまは使用する予定なし                        
            Jsonpath = Jsonpath + "arm/";
        }
        public Transform getArmTF()
        {
            return armTF;
        }

    }
}
