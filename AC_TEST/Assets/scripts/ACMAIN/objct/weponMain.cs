﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Text;

namespace Assets.scripts.objct
{

    [Serializable]
    public class weponMain:ACObjectMain
    {
            public int power;
            public zokusei element;
            public int ammos;
            public float sousasei;
            public float speed;
            public int ammoPrice;
            public int delTime;//対空期間
            public float relodeTime;

        private string layerstr;
        //            public int yuudou;//継承先で設定する
  //      [NonSerialized]
        [SerializeField]
        GameObject ammo;
        [NonSerialized]
        ammoManeger ammoM;
        [NonSerialized]
        Transform wepTF;
        [NonSerialized]
        private float interval;

        public void setWepTF(Transform wepontransform)
        {
            wepTF = wepontransform;
        }
        protected override void starts()
        {
            ammoM = GameObject.Find("ammos").GetComponent<ammoManeger>();
            //base.starts();//いまは使用する予定なし                        
            Jsonpath = Jsonpath + "wepon/";
            interval = 100;
            layerstr = LayerMask.LayerToName( wepTF.gameObject.layer);
        }

        virtual public void weponHanger()
        {
            interval = 100;
        }


        virtual public void weponUpdate()
        {
            interval += Time.deltaTime;
           }

        virtual public void weponAtack()
        {
            if (relodeTime < interval)
            {
                interval = 0;
                ammoMain.ammoPara_t buff = new ammoMain.ammoPara_t();
                buff.speed = speed;
                buff.deltime = delTime;
                buff.size = new Vector3(1, 1, 1);
                ammoM.makeAmmo(wepTF, wepTF.rotation, buff, layerstr);
            }

        }
        public Transform getWepTF()
        {
            return wepTF;
        }
    }
}
