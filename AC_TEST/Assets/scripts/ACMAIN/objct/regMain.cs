﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.scripts.systems;

namespace Assets.scripts.objct
{
    [Serializable]
    public class regMain : freamer
    {
        public int MaxHevy;
        public int balanser;
        public float walkSpeed;
        public float jump;
        public int motion;//運動性

        public float jumpTime { get; set; }
        public float notMoveJumpTime { get; set; }
        public float boostTime { get; set; }
        public float notMoveBoostTime { get; set; }
//        public float stopTime { get; set; }
        public float notMoveStopTime { get; set; }

        //        protected float jumpInterval;

        private enum regFlag { non, walk, boost, jump }
        private regFlag flag;



        protected override void starts()
        {
            //base.starts();//いまは使用する予定なし                        
            flag = regFlag.non;
            //            jumpInterval = 0;
        }
        public override void jsonStart()
        {
            base.jsonStart();
            Jsonpath = Jsonpath + "reg";
        }

        virtual public void regUpdate()
        {
        }
        //コルーチンなら楽だゾー//speedはブースター等の補助性能？
        virtual public Vector3 regWork(Vector3 vec, float speed, float time)
        {

            return vec;
        }
        virtual public Vector3 regBooster(float intervalMax, float interval, float speed,Vector3 inputVec)
        {
            var vec = Vector3.zero;
            vec.x = inputVec.x * ( speed / intervalMax);
            vec.z = inputVec.z * ( speed / intervalMax);
            return vec;
        }
        //計算式を持つのみ
        virtual public Vector3 regJump(float intervalMax, float interval, float speed)
        {
            //基本式
            var vec = Vector3.zero;
            vec.y =speed/intervalMax;
            return vec;
        }
    }
}
