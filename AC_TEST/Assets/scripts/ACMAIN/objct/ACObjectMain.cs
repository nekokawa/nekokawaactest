﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.scripts.Systems;
using UnityEngine;

namespace Assets.scripts.objct
{
    /// <summary>
    /// MonoBehaviourの継承は不要で必要なもののみGameObjectを取得した方がいいかも
    /// </summary>
    [Serializable]
    public class ACObjectMain
        {
        public int id;
        public string name;
        public int weight;
        public int losEN;

        public string Jsonpath;


        public void Start()
        {
            //            difenceData.ap = 10;
            starts();
            jsonStart();
        }
        virtual public void jsonStart()
        {
            Jsonpath = "object/AC/";
        }
        virtual protected void starts()
        {
        }

        virtual public void Act()
        {

        }

        virtual public void ACobjUpdate()
        {

        }

        //元で使いましょう
        //virtual protected bool jsonLoad(string Jsonpath, out ACObjectMain result)
        //{
        //    var IOmain = new JsonIO();
        //    return IOmain.loadJson<ACObjectMain>(Jsonpath, out result);
        //}
        //virtual protected void jsonsave()
        //{
        //    var IOmain = new JsonIO();
        //}

    }
}
