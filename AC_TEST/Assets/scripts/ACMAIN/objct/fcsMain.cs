﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.scripts.ACMAIN.systems;
using Assets.ACMAIN.Script.systems;

namespace Assets.scripts.objct
{
    //fcsはカメラobjに？
    [Serializable]
    public class fcsMain:ACObjectMain
    {
            public float size;//非優先ロックサイズ
            public float speed;
            public int maxNom;//同時ロック数
            public float firstSize;//優先ロックサイズ
            public float distance;//kyori 

        private Transform fcsTF;

        private struct lockCheckData
        {
           public Transform pos;
           public float lockTime;
        }
        private Transform mytf;//たぶんプレイヤー値（腕からでなくプレイヤーからロックする）カメラの親かもしれない
//        private List<lockCheckData> lockonObj;//現在たくさんのロックは不要
        private Transform targetObj;//ロックオンした場所
        [SerializeField]
        public GameObject mng;

        IObjectManager objManager;

        bool targetFlag;

        public void setFcsTF(Transform fcsTransform)
        {
            fcsTF = fcsTransform;
        }

        protected override void starts()
        {
            base.starts();
//            lockonObj = new List<lockCheckData>();
//            objManager = mng.GetComponent<IObjectManager>();
            targetObj = mytf;

            targetFlag = false;
            //distance = 10000;
            firstSize = 60.0f;
            //maxNom = 0;
            //size = 0;
            //speed = 0;
        }
        public override void jsonStart()
        {
            base.jsonStart();
            Jsonpath += "fcs";
        }
        public void fcsUpdate()
        {
            test();
        }
        //過去ロックシステム
        ///// <summary>
        ///// 新規ロック対象チェック
        ///// </summary>
        //private void lockCheckNew()
        //{
        //    var distance1 = (distance / 100);
        //    for (int i=0;i<objManager.getTransforms().Count();i++)
        //    {
        //        var tfBuff = objManager.getTransforms().Skip(i).First();
        //        var distaceBuff = (tfBuff.position - mytf.position).magnitude;
        //        if (distaceBuff < distance1)
        //        {
        //            if(!lockonObj.Any(x=>x.pos == tfBuff))//ここ重くなる可能性大
        //            {
        //                var buff = new lockCheckData();
        //                buff.pos = tfBuff;
        //                buff.lockTime = 0;
        //                lockonObj.Add(buff);
        //            }
        //        }
        //    }
        //}
        ///// <summary>
        ///// いまだにlock対象かどうかをチェックする
        ///// </summary>
        //private void lockCheck()
        //{
        //    var imax = lockonObj.Count();
        //    var distance1 = (distance / 100);
        //    for (int i=0;i<imax;i++)
        //    {
        //        var distaceBuff = (lockonObj[i].pos.position - mytf.position).magnitude;//チェック中の敵と自分の差分距離
        //        var distaceBuff2 = (targetObj.position - mytf.position).magnitude;//チェック中の敵とターゲットにしてる敵との差分距離
        //        if(distaceBuff > distance1)
        //        {
        //            lockonObj.RemoveAt(i);
        //            Debug.Log("not target");
        //            continue;
        //        }
        //        if (distaceBuff2 >= distaceBuff || targetObj == mytf)
        //        {
        //            if(targetCheck(lockonObj[i].pos.position))
        //            {
        //                //最も近くてターゲット内ならそれをターゲットに
        //                targetObj = lockonObj[i].pos;
        //            }
        //        }
        //    }
        //}
        private void test()
        {
            var buff = objManager.getTransforms().ToArray();
            var imax = buff.Count();
            var result = false;
            for (int i = 0; i < imax; i++)
            {
                if (targetCheck(buff[i].position))
                {
                    targetObj = buff[i];
                    result = true;
                }
            }
            if(result)
            {
                targetFlag = true;
            }
            else
            {
                targetFlag = false;
            }
        }
        /// <summary>
        /// 最優先ターゲットをチェックする
        /// </summary>
        private bool targetCheck(Vector3 enemyPos)
        {
            var result = false;
            if(Vector3.Angle((enemyPos- mytf.position).normalized,mytf.forward)<=firstSize)
            {
                result = true;
            }
            return result;
        }

        public Transform getTargetObj(out bool result)
        {
            if (!targetFlag)
            {
                result = false;
                return targetObj;
            }
            result = true;
            return targetObj;
        }
        public Transform getFcsTF()
        {
            return fcsTF;
        }
    }
}
