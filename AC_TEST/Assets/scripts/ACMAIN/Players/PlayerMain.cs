﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.scripts.objct;
using Assets.scripts.systems;
using UnityEngine.UI;
using Assets.scripts.ACMAIN.playerControll;
using Assets.ACMAIN.Script.systems;

public class PlayerMain : MonoBehaviour,IObjectManager {

    //    private List<GameObject> ListGames;
    Transform myTF;
    [SerializeField]
    Image ENgage;
    [SerializeField]
    Image APGage;
    [SerializeField]
    float mouseKando=0.1f;
    //    [SerializeField]
    //    playerMove PMove;
    //    [SerializeField]
    //    weponMain Pshot;
    //    [SerializeField]
    //    playerLotate Plotate;

    //    [SerializeField]
    //    armMain Rarm;
    //    [SerializeField]
    //    armMain Larm;

    //    [SerializeField]
    //    fcsMain FCS;

    //    public kitai_t status;
    //    private kitai_t statusOrign;

    //    private playerLock Plock;

    //	// Use this for initialization
    //	void Start ()
    //    {
    //        Plock = new playerLock(Rarm, Larm,FCS);
    //        status.fream.main_ap = 10;
    //        statusOrign = status;
    //	}

    //	// Update is called once per frame
    //	public void playerUpdate () {

    //       // PMove.moveScript();

    //        Plock.lockUpdate();
    //        if (Input.GetButton("Fire1"))
    //        {
    ////            Pshot.weponUpdate();        //本当はハンガーとか考えないといけない
    //        }
    //        Plotate.lotateUpdate();



    //    }

    //    public void IDamager(int atk)
    //    {
    //        status.fream.main_ap -= atk;
    //        APGage.fillAmount = ((float)status.fream.main_ap / (float)statusOrign.fream.main_ap);
    //        if (status.fream.main_ap <= 0)
    //            Debug.Log("END");
    //    }
    //    public void IMover()
    //    {
    //        PMove.moveScript();
    //    }
    //    public void IhasLock(Transform enemyPos)
    //    {

    //    }

    [SerializeField]
     ACMain acCntrol;
    
    public void Start()
    {
        myTF = transform;
        acCntrol.findACDatas();
        acCntrol.gage(APGageChange, ENGageChange);
    }

    public void playerUpdate()
    {
        acCntrol.acUpdate();

        acCntrol.ACcontrol(new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"))
            , Input.GetAxis("Mouse X")* mouseKando, Input.GetAxis("Mouse Y")* mouseKando*-1
            ,Input.GetButton("Boost")
            ,Input.GetButton("RShot"), Input.GetButton("LShot"),Input.GetButton("RSShot"), Input.GetButton("LSShot")
            , Input.GetButton("HngerChange") && Input.GetButton("RShot"), Input.GetButton("HngerChange") && Input.GetButton("LShot")
            , Input.GetButton("Parge") && Input.GetButton("RShot"), Input.GetButton("Parge") && Input.GetButton("LShot")
            );
    }

    public IEnumerable<Transform> getTransforms()
    {
        yield return myTF;
    }

    public bool endCheck()
    {
        if(acCntrol.hp<=0)
            return true;
        return false;
    }
    private void APGageChange(int max,int now)
    {
        APGage.fillAmount = ((float)now/(float)max);
    }
    private void ENGageChange(int max,int now)
    {

    }
}
