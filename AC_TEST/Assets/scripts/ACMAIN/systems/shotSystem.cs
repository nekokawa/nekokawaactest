﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.scripts.ACMAIN.systems
{
    class shotSystem
    {
        public Quaternion nextShot(Vector3 myPos,Vector3 nowPos,Vector3 nextPos,float ammoSpeed)
        {
            float buff=0;
            var result = new Quaternion();
            float enemySpeed = (nowPos - nextPos).magnitude;
            float kyori = (nextPos - myPos).magnitude;
            var ang = Quaternion.FromToRotation(nowPos,nextPos);

            buff = kyori / ammoSpeed;

            buff = buff * enemySpeed;
            //ここで相手の未来位置を予測
            nowPos += (nowPos - nextPos).normalized*buff;

            result = Quaternion.FromToRotation(myPos,nowPos);
            return result;
        }
    }
}
