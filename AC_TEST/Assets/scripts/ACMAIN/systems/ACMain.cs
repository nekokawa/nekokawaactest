﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.scripts.objct;

namespace Assets.ACMAIN.Script.systems
{
    class ACMain:MonoBehaviour,IhasDamager
    {
        public delegate void myIntReturn(int max,int now);
        myIntReturn myApReturn;
        myIntReturn myEnReturn;
        private ACset ACDatas;//これもっとく必要ないかも
                              //test
        [SerializeField]
        headMain head;
        [SerializeField]
        coreMain core;
        [SerializeField]
        armMain Larm;
        [SerializeField]
        armMain Rarm;
        [SerializeField]
        weponMain Rwep;
        [SerializeField]
        weponMain RSwep;
        [SerializeField]
        weponMain Lwep;
        [SerializeField]
        weponMain LSwep;
        [SerializeField]
        List<weponMain> hangerwep;
        [SerializeField]
        regMain reg;
        [SerializeField]
        fcsMain fcs;
        [SerializeField]
        boosterMain booster;
        [SerializeField]
        generatorMain gene;

        [SerializeField]
        Rigidbody myRB;

        public moveMain moveM;
        shotMain shotM;
        

        //合計値データ
        public int hp;
        private int kdef;
        private int edef;
        
        public void findACDatas()
        {
            //本来は
            //保存データ読み込み
            //パスからプレファブ取得
            //フレームデータに合わせてポジション調整
            //データを当てはめていく

            //ACDatas.head = GameObject.Find("").GetComponent<headMain>();
            //ACDatas.Larm = GameObject.Find("").GetComponent<armMain>();
            //ACDatas.Rarm = GameObject.Find("").GetComponent<armMain>();
            //ACDatas.core = GameObject.Find("").GetComponent<coreMain>();
            //ACDatas.reg= GameObject.Find("").GetComponent<regMain>();
            //ACDatas.booster = GameObject.Find("").GetComponent<boosterMain>();
            //ACDatas.fcs = GameObject.Find("").GetComponent<fcsMain>();
            fcs.Start();
            moveM = new moveMain(myRB,reg,booster,gene,fcs.getFcsTF());
            shotM = new shotMain(fcs,Rarm,Larm,Rwep,Lwep,RSwep,LSwep,hangerwep);

            hp = core.ap;
        }
        public void gage(myIntReturn apMethod,myIntReturn enMethod)
            {
            myApReturn = apMethod;
            myEnReturn = enMethod;
            }
        private void makeHP<T>()
            where T :freamer
        {
            
        }
        //コントローラーのつもりだがやばいかも。一応今は毎回読み込む予定
        //オプション引数いるぅ？(dllに問題がある)
        public void ACcontrol(
            Vector3 moveAxs,Quaternion rotateAng, bool boost //intじゃにゃいか？
            , bool Lshot,bool Rshot,bool RHshot,bool LHshot
            ,bool changeLshot, bool changeRshot , bool pargeLshot, bool pargeRshot
            )
        {
            ACmove(moveAxs, boost);
            ACrotate(rotateAng);
            ACShot(Lshot, Rshot, RHshot, LHshot);
            AChngChange(changeLshot,changeRshot,pargeLshot,pargeRshot);
        }
        public void ACcontrol(
    Vector3 moveAxs, float x ,float y, bool boost //intじゃにゃいか？
    , bool Lshot , bool Rshot , bool RHshot , bool LHshot 
    , bool changeLshot , bool changeRshot , bool pargeLshot , bool pargeRshot 
    )
        {
            ACmove(moveAxs, boost);
            ACShot(Lshot, Rshot, RHshot, LHshot);
            ACrotate(x,y);
            AChngChange(changeLshot, changeRshot, pargeLshot, pargeRshot);
        }

        public void ACmove(Vector3 moveAxs, bool boost = false)
        {
            if (boost)
            {
                if (moveAxs != Vector3.zero)
                {
                    //ブースト移動
                    moveM.moveBoost(moveAxs);
                }
                else if (moveAxs == Vector3.zero)
                {
                    //ジャンプ
                    moveM.jump();
                }
            }
            else
            {
                if (moveAxs != Vector3.zero)
                {
                    //歩行移動
                    moveM.movePlane(moveAxs);
                }
            }
            moveM.moveUpdate();//移動減速とか
        }
        public void ACrotate( Quaternion rotateAng)
        {
            moveM.rotate(rotateAng);
        }
        public void ACrotate(float x,float y)
        {
            moveM.rotate(x, y);
        }
        public void ACShot(bool Lshot = false, bool Rshot = false, bool RHshot = false, bool LHshot = false)
        {
            shotM.shotUpdate();
            if(Rshot)
            {
                 shotM.shotTriggerR(1);
            }
            if (Lshot)
            {
                shotM.shotTriggerR(2);
            }
            if (RHshot)
            {
                shotM.shotTriggerR(3);
            }
            if (LHshot)
            {
                shotM.shotTriggerR(4);
            }
        }
        public void AChngChange(bool changeLshot = false, bool changeRshot = false, bool pargeLshot = false, bool pargeRshot = false)
        {

        }

        public void acUpdate()
        {
            moveM.moveUpdate();
            shotM.shotUpdate();
        }


        public void IDamager(int atk)
        {
            hp -= atk;
            if(hp <= 0)
            {
                //ですトロイ処理
                Debug.Log("end");
            }
            myApReturn(core.ap,hp);
        }
    }
}
