﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.scripts.objct;

namespace Assets.ACMAIN.Script.systems
{
    //移動は加速度を使う
    class moveMain
    {
        public enum moveFlag{
            stop,
            moving,
            stoping,
            jumping,
            jump,
            boost,
            boosting
        };
        private Rigidbody mainRB;
        private regMain reg;
        private boosterMain booster;
        private generatorMain generator;

        private struct flagTime
        {
            public float max;
            public float notMoveTime;//実質入力不可時間
            public float interval;
            public float speed;
            public bool flag;
        }
        private flagTime boostTime;
        private flagTime stopTime;
        private flagTime jumpTime;
        private bool isNotMove;
        private Vector3 lastInput;
        private float intervalMax;//現フラグの最終段階も示す

        private float interval;//現フラグがどんだけ続いているかもあらわす

        private Transform fcs;

        private moveFlag flag;
        public bool isGrand;//これは他からとってくるべき？
        private Vector3 moveDirection;//ようはベクトル

        //weponnの移動はweponの処理で行う
        //初期化
        public moveMain(Rigidbody moveTF, regMain regM, boosterMain boost, generatorMain gene,Transform fcsM)
        {
            mainRB = moveTF;
            reg = regM;
            generator = gene;
            booster = boost;
            fcs = fcsM;

            var timeBuff = new flagTime { interval = 0, flag = false, notMoveTime = 0 };
            boostTime =timeBuff;
            jumpTime = timeBuff;
            stopTime = timeBuff;
            boostTime.max = regM.boostTime + boost.notMoveTime;
            boostTime.notMoveTime = regM.notMoveBoostTime + boostTime.notMoveTime/2.0f;
            boostTime.speed = booster.mainboost / 15f;
            jumpTime.max = regM.jumpTime;
            jumpTime.notMoveTime = regM.notMoveJumpTime;
            jumpTime.speed = (reg.jump + booster.upboost);
            stopTime.max = regM.notMoveStopTime;
            stopTime.notMoveTime = stopTime.max;

            lastInput = Vector3.zero;
            flag = moveFlag.stop;
            interval = 100;
            isGrand = true;
            isNotMove = false;
            moveDirection = Vector3.zero;
            mainRB.mass = 10;
        }

        //落ちたりとか減速処理とか
        public void moveUpdate()
        {
            interval += Time.deltaTime;
            isGrand = checkGrand();
            if (isGrand)
            {
            }
            else
            {
            }

            checkFlag();
            var cmrforw = Vector3.Scale(fcs.forward, new Vector3(1, 0, 1)).normalized;
            var buffy = moveDirection.y;
            moveDirection= cmrforw * moveDirection.z + fcs.right * moveDirection.x;
            mainRB.MovePosition(mainRB.position + moveDirection*Time.deltaTime*10);
            mainRB.AddForce(new Vector3(0, buffy ));
            moveDirection = Vector3.zero;
        }
        private bool checkGrand()
        {
            ////マイナスの時のみ更新
            //var buff = mainRB.velocity.y;
            //if (buff >= 0 && buff <= 1)
            //{

            //}
            var raybuf = new Ray(mainRB.position,Vector3.down);
            return !Physics.SphereCast(raybuf,3,5);
        }
        /// <summary>
        ///各フラグのチェックです
        /// </summary>
        private void checkFlag()
        {
            if(boostTime.flag)
            {
                moveDirection += reg.regBooster(boostTime.max,boostTime.interval,boostTime.speed,lastInput);
                boostTime = checkFlagBuff(boostTime);
            }
            else if(jumpTime.flag)
            {
                moveDirection += reg.regJump(jumpTime.max,jumpTime.interval,jumpTime.speed);
                jumpTime = checkFlagBuff(jumpTime);
            }
            else if (stopTime.flag)
            {
                stopTime = checkFlagBuff(stopTime);
            }
        }
        private flagTime  checkFlagBuff(flagTime buff)
        {
            buff.interval += Time.deltaTime;
            if(isNotMove && buff.notMoveTime<buff.interval)
            {
                isNotMove = false;
            }
            if (buff.max < buff.interval)
            {
                buff.flag = false;
            }
            return buff;
        }

        public void movePlane(Vector3 angvec)
        {
            if (isNotMove)
            {
            }
            else if(boostTime.flag)
            {
                lastInput = angvec;
            }
            else
            {
                moveDirection.x += angvec.x * reg.walkSpeed/10;//歩行速度
                moveDirection.z += angvec.z * reg.walkSpeed/10;
            }
        }
        /// <summary>
        /// angvec=isNotZero
        /// </summary>
        /// <param name="angvec"></param>
        public void moveBoost(Vector3 angvec)
        {
            if (boostTime.flag)
            {
                lastInput = angvec;
                boostTime.interval = 0;
            }
            else if(isNotMove)
            {
            }
            else
            {
                boostTime.flag = true;
                isNotMove = true;
                boostTime.interval = 0;
                lastInput = angvec;
                ////ここにregからブーストの処理
                //moveDirection.x += angvec.x * booster.mainboost;
                //moveDirection.z += angvec.z * booster.mainboost;
            }
        }
        public void jump()
        {
            if (isNotMove)
            {

            }
            else if (!isGrand)
            {
                //ブースト上昇処理
                moveDirection.y += booster.upboost;
            }
            else
            {
                jumpTime.flag = true;
                jumpTime.interval = 0;
                isNotMove = true;
            }
        }
        public void rotate(Quaternion rota)
        {
            //本当はカメラ回転と機体回転は別。機体回転はカメラの方向へゆっくり
            mainRB.rotation = Quaternion.Euler(0,rota.eulerAngles.y,0);
            fcs.localEulerAngles = new Vector3(rota.eulerAngles.x,0,0);
        }
        public void rotate(float x,float y)
        {
            mainRB.rotation *= Quaternion.AngleAxis(x, Vector3.up);

            fcs.Rotate(y,0,0);
            if (fcs.localEulerAngles.x > 30 && fcs.localEulerAngles.x <300)
            {
                if (y>0)
                    fcs.localEulerAngles = new Vector3(30, 0, 0);
                else if(y<0)
                    fcs.localEulerAngles = new Vector3(300, 0, 0);
            }
        }

        public moveFlag getFlag()
        {
            return flag;
        }
        public bool getIsGrand()
        {
            return isGrand;
        }

    }
}
