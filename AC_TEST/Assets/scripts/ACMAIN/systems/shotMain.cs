﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.scripts.objct;

namespace Assets.ACMAIN.Script.systems
{
    //構えているものは送られてくるweponで判断、更に左と右と肩でそれぞれこれを持っておくX
    //ここですべて管理すること
    //ディクショナリー管理の理由は構造体だと抜いたとき面倒だから（それ故にこのクラスは再利用性がとても低い）
    class shotMain
    {
        public enum shotFlag
        {
            non,
            charge,
            shot,
            hangerCahnge
        }
        private Dictionary<int,shotFlag> flag;

        private fcsMain fcs;
        private Dictionary<int,armMain> arms;

        //private transform Rarm;
        //private transform Larm;
        //private transform RSarm;
        //private transform LSarm;

        //        private List<weponMain> wepon;//追加予定はなし？

        private Dictionary<int, weponMain> weps;
        readonly int rwep = 1;
        readonly int lwep = 2;
        readonly int rswep = 3;
        readonly int lswep = 4;


        //各種必要ステータスデータ
        //private float intervalMax;//腕安定、ウェポンによって
        //private float interval;
        private float mooveing;//運動性
        private Dictionary<int,Vector3> lockPos;//ロックしている位置lookatで取得予定のため

        /// <summary>
        /// ウェポンは順番で送ってきて下しあ
        /// 0=R,1=L,2=RS
        /// </summary>
        /// <param name="fcsbuff"></param>
        /// <param name="weps"></param>
        public shotMain(fcsMain fcsbuff,armMain rarm,armMain larm
            ,weponMain Rwep,weponMain Lwep,weponMain RSwep,weponMain LSwep,List<weponMain> hangerWeps)
        {
            lockPos = new Dictionary<int, Vector3>();
            weps = new Dictionary<int, weponMain>();
            arms = new Dictionary<int, armMain>();
            flag = new Dictionary<int, shotFlag>();

            fcs = fcsbuff;
            if (Rwep != null)
            {
                startSetup(rwep, Rwep);
                arms.Add(rwep,rarm);
            }
            if (Lwep != null)
            {
                startSetup(lwep, Lwep);
                arms.Add(lwep, larm);
            }
            if (RSwep != null)
            {
                startSetup(rswep, RSwep);
            }
            if (LSwep != null)
            {
                startSetup(lswep, LSwep);
            }
            if (hangerWeps != null)
            {
                for (int i = 0; i < hangerWeps.Count(); i++)
                {
                    startSetup(i + 11, hangerWeps[i]);
                }
            }
        }
        private void startSetup(int wepNo,weponMain wep)
        {
            weps.Add(wepNo, wep);
            lockPos.Add(wepNo,Vector3.zero);
            flag.Add(wepNo,shotFlag.non);
        }
        
        public void shotUpdate()
        {
            fcs.fcsUpdate();
            foreach (int key in weps.Keys.Where(x => x < 10))
            {
                wepHndUpdate(weps[key]);
                weponsRotateUpdate(key);
            }
        }

        private void wepHndUpdate(weponMain wepbuff)
        {
            wepbuff.weponUpdate();
        }
        private void weponsRotateUpdate(int wepNo)
        {
            bool buff;
            Transform tfbuff = fcs.getTargetObj(out buff);
            if (buff)
            {
                lockPos[wepNo] = tfbuff.position;
                if (wepNo <= 2)
                {
                    var buffq = Quaternion.LookRotation(lockPos[wepNo] - arms[wepNo].getArmTF().position);
                    arms[wepNo].getArmTF().rotation=Quaternion.Slerp(arms[wepNo].getArmTF().rotation, buffq, Time.deltaTime*3);
//                    arms[wepNo].getMyTF().LookAt(lockPos[wepNo]);
                }
                else
                {
                    var buffq = Quaternion.LookRotation(lockPos[wepNo] - weps[wepNo].getWepTF().position);
                    weps[wepNo].getWepTF().rotation = Quaternion.Slerp(weps[wepNo].getWepTF().rotation, buffq, Time.deltaTime );
                }
//                weps[wepNo].getMyTF().LookAt(lockPos[wepNo]);
            }
            else
            {
                if (wepNo <= 2)
                {
                    arms[wepNo].getArmTF().rotation = Quaternion.Slerp(arms[wepNo].getArmTF().rotation, fcs.getFcsTF().rotation, Time.deltaTime * 3);
                    //                    arms[wepNo].getMyTF().rotation = Quaternion.Euler(Vector3.zero);
                }
                else
                {
                    weps[wepNo].getWepTF().rotation = Quaternion.Slerp(weps[wepNo].getWepTF().rotation, Quaternion.Euler(Vector3.zero), Time.deltaTime );
                    //                    weps[wepNo].getMyTF().rotation = Quaternion.Euler(Vector3.zero);
                }
            }
        }

        public void shotTriggerR(int wepNo)
        {
            if(weps.ContainsKey(wepNo))
                weps[wepNo].weponAtack();
        }
        //インスタンスなのにこれでおkだっけ？
        public void hangerChange(int wepNo1,int wepNo2)
        {
            weponMain buff = weps[wepNo1];
            weps[wepNo1] = weps[wepNo2];
            weps[wepNo2] = buff;
        }
        public void pargeWepon(int wepNo)
        {
            //weps[wepNo]//削除処理
        }

        public shotFlag getFlag(int wepNo)
        {
            return flag[wepNo];
        }
    }
}
