﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.scripts.objct;
using System.Reflection;

namespace Assets.scripts.ACMAIN.systems
{
    class reflectionLoad
    {
        /// <summary>
        /// リフレクションでクラスをStrから取得(事前にアセンブリロードすること)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="className">class名（.classは不要）</param>
        /// <param name="answer">out <T>を</T>返すよ</param>
        /// <returns>成功したかを返すよ</returns>
        public bool loadClass<T>(string className,out T answer)
            where T:class
        {
            bool result = true;
            try {
                Type strClass = Type.GetType(className);
                answer = (T)Activator.CreateInstance(strClass);                
            }
            catch
            {
                answer = null;
                result = false;
            }
            return result;
        }
        
    }
}
