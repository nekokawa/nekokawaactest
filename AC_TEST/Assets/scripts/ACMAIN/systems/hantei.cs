﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.scripts.systems
{
    class hantei
    {
        //今回はレイヤーマスクなし
        //rayでの問題点原点位置の取得ができない③HIT位置を返していない
        /// <summary>
        /// 二点間の判定(linecastみたいな形ただし返しは一つ)
        /// </summary>
        /// <param name="now">原点</param>
        /// <param name="next">終点</param>
        /// <param name="returncoll">当たった判定返す</param>
        /// <returns>成功したか</returns>
        public bool freamRev(Vector3 now,Vector3 next,LayerMask layerMask,out RaycastHit returncoll)
        {
            RaycastHit hit;
            Vector3 vectemp = next - now;
            float stans = (next-now).magnitude;
            Vector3 nomal = vectemp.normalized;
            //debugのため線を引いている
            Debug.DrawRay(now, nomal*stans,Color.red,stans,false);
            int buff= -1 -1 << layerMask.value;
            if(Physics.Raycast(now, nomal, out hit,stans,buff))
            {
                returncoll = hit;
                return true;
            }
            returncoll = hit;
            return false;
        }
        //linecast版を作成する？No理由は複数ほしい場合がないから


    }
}
