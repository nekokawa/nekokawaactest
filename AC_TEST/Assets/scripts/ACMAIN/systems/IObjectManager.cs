﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.ACMAIN.Script.systems
{
    interface IObjectManager
    {
        IEnumerable<Transform> getTransforms();
    }
}
