﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.scripts.objct;
using Assets.scripts.systems;


public class ammoManeger : MonoBehaviour {

    struct ammo_t
    {
        public GameObject ammoOBJ;
        public ammoMain.ammoPara_t ammo;
        public Transform myTF;
        public Transform youTF;//目標地点
        public float duraTime;
    }
    public GameObject efect;
    public GameObject ammo;
    private List<ammo_t> ammos = new List<ammo_t>();
    private hantei hanteikeitou = new hantei();

	//// Use this for initialization
	//void Start () {
		
	//}
	
	// Update is called once per frame
	public void ammoUpdate ()
    {
		for(int i=0;i<ammos.Count;i++)
        {
            ammo_t buff = ammos[i];
            buff.duraTime += Time.deltaTime;
            ammos[i]=buff;
            if (rayHantei(ammos[i]))
            {
                ammos.RemoveAt(i);
            }
        }
	}

    //ここは外から読みだしてもらう
    public void makeAmmo(Transform makeTF, Quaternion makeQua, ammoMain.ammoPara_t ammoData,string LayerName)
    {
        ammo_t buff = makeAmmoAlls(makeTF, makeQua, ammoData,LayerName);
        ammos.Add(buff);
    }
    public void makeAmmo(Transform makeTF, Quaternion makeQua, Transform enemyTF, ammoMain.ammoPara_t ammoData, string LayerName)
    {
        ammo_t buff = makeAmmoAlls(makeTF,makeQua, ammoData, LayerName);
        buff.youTF = enemyTF;
        ammos.Add(buff);
    }
    private ammo_t makeAmmoAlls(Transform makeTF, Quaternion makeQua, ammoMain.ammoPara_t ammoData, string LayerName)
    {
        ammo_t buff = new ammo_t();
        buff.ammoOBJ = GameObject.Instantiate(ammo, makeTF.position, makeQua);
        buff.ammo = ammoData;
        buff.duraTime = 0;
        buff.myTF = buff.ammoOBJ.GetComponent<Transform>();
        buff.ammoOBJ.layer = LayerMask.NameToLayer( LayerName );
        return buff;

    }


    private bool rayHantei(ammo_t ammoPara)
    {
        if (ammoPara.duraTime > ammoPara.ammo.deltime)
        {
            Destroy(ammoPara.ammoOBJ);
            return true;
        }
        bool result = false;//true=ヒット
        Vector3 now = ammoPara.myTF.position;
        ammoPara.myTF.position += ammoPara.myTF.forward * Time.deltaTime * ammoPara.ammo.speed;
        var collbuff = new RaycastHit();
        //Ray判定当たったら処理
        if (hanteikeitou.freamRev(now, ammoPara.myTF.position, ammoPara.ammoOBJ.layer, out collbuff))
        {
            Instantiate(efect, collbuff.point, Quaternion.identity);
            result = true;
                hit(collbuff);
//            if (result)
                Destroy(ammoPara.ammoOBJ);
        }
        return result;
    }
    private bool hit(RaycastHit collbuff)
    {
        bool result = false;
        if (collbuff.collider.GetComponent(typeof(IhasDamager)))
        {
            IhasDamager IHD = collbuff.collider.GetComponent(typeof(IhasDamager)) as IhasDamager;
            IHD.IDamager(1);
            Debug.Log("hit");
            result = true;
        }
        return result;
    }
}
