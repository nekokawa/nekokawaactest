﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMove : MonoBehaviour {
    const float gravite_con = 3.0f;
    private float speed = 3.0f;
    private float jumpSpeed = 5.0f;
    private float jumpDura = 0;
    private float jumpMin = 0.15f;
    private float gravity = gravite_con;
    private Vector3 moveDirection_vec = Vector3.zero;
    //キャッシュしておくでないと毎回ゲットコンポーネントすることに
    private Transform myTF;

	// Use this for initialization
	void Start () {
        myTF = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	public void moveScript()
    {
    Vector3 moveDirection = Vector3.zero;
    CharacterController controller = GetComponent<CharacterController>();
        //ジャンプ制御(本来はジャンプ制御不可期間を設ける)
        if(0<jumpDura && jumpDura<jumpMin)
        {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moveDirection = myTF.TransformDirection(moveDirection);
            moveDirection *= speed;
            moveDirection *= 1.5f;
            moveDirection.y = jumpSpeed * 2.0f;
            jumpDura += Time.deltaTime;
        }
        //地上制御
        else if (true)
        {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moveDirection = myTF.TransformDirection(moveDirection);
            moveDirection *= speed;
            if(Input.GetButton("Jump"))
            {
                moveDirection *= 1.5f;
                moveDirection.y = jumpSpeed*3.0f;
                jumpDura = 0.0001f;                
            }
        }
        //空中制御
        else
        {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moveDirection = myTF.TransformDirection(moveDirection);
            moveDirection *= speed;
            if (Input.GetButton("Jump"))
            {
                moveDirection.y += jumpSpeed ;
            }
        }
        moveDirection_vec += moveDirection;
        moveDirection_vec = moveDirection_vec / 1.1f;
        moveDirection_vec.y -= gravity;
        this.GetComponent<Rigidbody>().position=moveDirection_vec * Time.deltaTime;
	}
}
