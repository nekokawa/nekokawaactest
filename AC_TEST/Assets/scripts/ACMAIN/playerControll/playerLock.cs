﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Assets.scripts.objct;

namespace Assets.scripts.ACMAIN.playerControll
{
    class playerLock
    {
        private armMain Larm;
        private armMain Rarm;
        private weponMain Lwepon;//主に誘導を見る
        private weponMain Rwepon;
        
        public fcsMain fcs;


        public playerLock(armMain Rarmbuff,armMain Larmbuff,fcsMain fcsbuff)
        {
            Rarm = Rarmbuff;
            Larm = Larmbuff;
            fcs = fcsbuff;
        }


        public void lockUpdate()
        {
            fcs.fcsUpdate();
            bool f;
            var buff = fcs.getTargetObj(out f);
            if (f)
            {
                Rarm.getArmTF().LookAt(buff);
                Larm.getArmTF().LookAt(buff);
            }
            else
            {
                Rarm.getArmTF().rotation= Quaternion.Euler(Vector3.zero);
                Larm.getArmTF().rotation =Quaternion.Euler(Vector3.zero);
            }
            //arm.Rotate();
        }


    }
}
