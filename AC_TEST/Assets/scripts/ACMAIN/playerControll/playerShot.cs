﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerShot : MonoBehaviour
{
    private float intarval = 1;
    private float intarvalMax = 0.2f;

    //本来はコードで取ってくるべき
    public GameObject shot;
    public GameObject ammoManegerOBJ;
    [SerializeField]
    Transform weponObj;


    // Use this for initialization
    //void Start () {
    //    CameraOBJ = Camera.main.transform.parent.gameObject;
    //}

    // Update is called once per frame
    public void shotUpdate()
    {
        intarval += Time.deltaTime;
        if (Input.GetButton("Fire1"))
        {
            if (intarvalMax < intarval)
            {
                ammoMain.ammoPara_t buff = new ammoMain.ammoPara_t();
                buff.speed = 100;
                buff.deltime = 2;
                buff.size = new Vector3(1,1,1);
                ammoManegerOBJ.GetComponent<ammoManeger>().makeAmmo(transform, weponObj.rotation, buff, "player_ammo");

                //var obj = Instantiate(shot, transform.position, CameraOBJ.transform.rotation);
                //obj.layer = LayerMask.NameToLayer("player_ammo");

                intarval = 0;
            }
        }
    }
}
