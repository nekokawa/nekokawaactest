﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerLotate : MonoBehaviour {
    const int MAX_ROTE_UD = 90;
    private float cameraRoteX;
    private GameObject CameraOBJ;
    // Use this for initialization
    void Start () {
        cameraRoteX = 0;
        CameraOBJ = Camera.main.transform.parent.gameObject;
	}

    // Update is called once per frame
    public void lotateUpdate()
    {
        //プレイヤーの回転
        transform.Rotate(0, Input.GetAxis("Horizontal2"), 0);
        //カメラの回転

        if (cameraRoteX <= MAX_ROTE_UD && cameraRoteX >= -MAX_ROTE_UD)
        {
            CameraOBJ.transform.Rotate(-Input.GetAxis("Vertical2"), 0, 0);
        }
    }
}
