﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.script.squares
{
    //まだ計算入れてない
    public class SquaresManagerSecond : MonoBehaviour
    {
        enum FAZE { load, start, firstChoose, secondChoose, move, attak, end,range,Line };
        readonly private float RAY_LONG = 30.0f;
        [SerializeField]
        int _makesX;
        [SerializeField]
        int _makesY;
        [SerializeField]
        GameObject _makeGameObject;
        [SerializeField]
        Transform _makeParentTF;
        [SerializeField]
        Camera _mainCamera;

        SquerMain[][] _hexSquares;
        SquerMain _targetSquer;
        private FAZE _squaresFaze;

        //フィールド作成
        void Start()
        {
            _squaresFaze = FAZE.load;
            var makeBuff = new MakeHexa();
            if(!makeBuff.MakeHexaSquears(_makesX, _makesY, _makeGameObject, _makeParentTF,out _hexSquares))
            {
                Debug.Log("end");
            }
            _squaresFaze = FAZE.firstChoose;
        }

        void Update()
        {
            if (IsControlFaze())
            {
                if (_squaresFaze == FAZE.firstChoose
                    || _squaresFaze == FAZE.secondChoose)
                {
                    ChooseOneObject();
                }
                else if (_squaresFaze == FAZE.range)
                {
                    ChooseRangeObject();
                }
                else
                {
                    ChooseLineObject();
                }
                if (Input.GetMouseButtonDown(1))
                {
                    ModeChange();
                }
            }
        }
        private void ChooseOneObject()
        {
            if (Input.GetMouseButtonDown(0))
            {
                var rayBuff = _mainCamera.ScreenPointToRay(Input.mousePosition);
                var hitBuff = new RaycastHit();
                if (Physics.Raycast(rayBuff.origin, rayBuff.direction, out hitBuff, RAY_LONG))
                {
                    if (hitBuff.collider.GetComponent(typeof(SquerMain)))
                    {
                        if (_squaresFaze == FAZE.firstChoose)
                        {
                            _targetSquer = hitBuff.collider.GetComponent<SquerMain>();
                            _targetSquer.IClick();
                            _squaresFaze = FAZE.secondChoose;
                        }
                        else if(hitBuff.collider.GetComponent<SquerMain>() == _targetSquer)
                        {
                            _targetSquer.IClickCancel();
                            _squaresFaze = FAZE.firstChoose;
                        }
                    }
                }
            }
        }
        private void ChooseRangeObject()
        {

        }
        private void ChooseLineObject()
        {

        }
        private void ModeChange()
        {
            if (_squaresFaze == FAZE.firstChoose
                || _squaresFaze == FAZE.secondChoose)
                _squaresFaze = FAZE.range;
            else if (_squaresFaze == FAZE.range)
                _squaresFaze = FAZE.Line;
            else
                _squaresFaze = FAZE.firstChoose;
            AllCeler();
        }
        private void AllCeler()
        {
            for (int i = 0; i < _hexSquares.Count(); i++)
                for (int j = 0; j < _hexSquares[i].Count(); j++)
                    _hexSquares[i][j].IClickCancel();
        }
        private bool IsControlFaze()
        {
            return (_squaresFaze == FAZE.firstChoose
                || _squaresFaze == FAZE.secondChoose
                || _squaresFaze == FAZE.range
                || _squaresFaze == FAZE.Line
                );
        }
    }
}