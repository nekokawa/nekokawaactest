﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.script.squares
{
    class SquerMain:MonoBehaviour,IHasClicker
    {
        private readonly Color CHANGE_COLOER = new Color(0,0,0);
        private GameObject _gameObjectBuff;
        private MeshRenderer _meshRenderBuff;
        private Color _nomalColoer;
        
        void Start()
        {
            _gameObjectBuff = gameObject;
            _meshRenderBuff = _gameObjectBuff.GetComponent<MeshRenderer>();
            _nomalColoer = _meshRenderBuff.material.color;
            SquaresStart();
        }
        virtual protected void SquaresStart(){}

        void Update(){ SquaresUpdate(); }
        virtual protected void SquaresUpdate() { }
        
        
       public void IClick() { _meshRenderBuff.material.color = CHANGE_COLOER; Debug.Log(name+"Click"); }
       public void IClickCancel() { _meshRenderBuff.material.color = _nomalColoer; }
    }
}
