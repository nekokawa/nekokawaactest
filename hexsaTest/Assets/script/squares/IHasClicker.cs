﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.script.squares
{
    interface IHasClicker
    {
        void IClick();
        void IClickCancel();
    }
}
