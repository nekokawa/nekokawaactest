﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.script.squares
{
    public class SquaresManager : MonoBehaviour
    {
        enum FAZE {load,start,firstChoose,secondChoose,move,attak,end,range,Line };
        readonly private float RAY_LONG = 30.0f;
        [SerializeField]
        int _makesX;
        [SerializeField]
        int _makesY;
        [SerializeField]
        GameObject _makeGameObject;
        [SerializeField]
        Transform _makeParentTF;
        [SerializeField]
        Camera _mainCamera;
        
        List<SquerMain> _hexSquares;
        SquerMain _targetSquer;

        private FAZE _squaresFaze;
        private HexMath _hexMath;

        //フィールド作成
        void Start()
        {
            _squaresFaze = FAZE.load;
            var makeBuff = new MakeHexa();
            _hexSquares = makeBuff.MakeHexaSquears(_makesX, _makesY, _makeGameObject, _makeParentTF).ToList();
            _hexMath = new HexMath(_makesX, _makesY, _hexSquares.Count);
            _squaresFaze = FAZE.firstChoose;            
        }
        void Update()
        {
            if (!IsControlFaze())
            {
                return;
            }
            if (Input.GetMouseButtonDown(0))//クリック後
            {

                var rayBuff = _mainCamera.ScreenPointToRay(Input.mousePosition);
                var hitBuff = new RaycastHit();
                if (Physics.Raycast(rayBuff.origin, rayBuff.direction, out hitBuff, RAY_LONG))
                {
                    if (hitBuff.collider.GetComponent(typeof(SquerMain)))//クリック位置にSquerMainがHit下なら
                    {
                        if (_squaresFaze == FAZE.firstChoose
                         || _squaresFaze == FAZE.secondChoose)//第一モード
                        {
                            ChooseOneObject(hitBuff.collider.GetComponent<SquerMain>());
                        }
                        else if (_squaresFaze == FAZE.range)//題にモード
                        {
                            ChooseRangeObject(hitBuff.collider.GetComponent<SquerMain>());
                        }
                        else//第三モード
                        {
                            ChooseLineObject(hitBuff.collider.GetComponent<SquerMain>());
                        }
                    }
                }
            }
            else if (Input.GetMouseButtonDown(1))
            {
                ModeChange();
            }

        }
        private void ChooseOneObject(SquerMain squerMainBuff)//単一選択
        {
            if (_squaresFaze == FAZE.firstChoose)
            {
                _targetSquer = squerMainBuff;
                _targetSquer.IClick();
                _squaresFaze = FAZE.secondChoose;
            }
            else if (squerMainBuff == _targetSquer)
            {
                _targetSquer.IClickCancel();
                _squaresFaze = FAZE.firstChoose;

            }
        }
        private void ChooseRangeObject(SquerMain squerMainBuff)//範囲選択
        {
            int i;
            if (TargetNo(squerMainBuff, out i))
            {
                var changeNos = _hexMath.RangeX(i,3).ToList();
                    for (int j = 0; j < changeNos.Count(); j++)
                            _hexSquares[changeNos[j]].IClick();
                }
        }
        private void ChooseLineObject(SquerMain squerMainBuff)//直線選択
        {
            int i;
            if(TargetNo(squerMainBuff,out i))
                {
                    var changeNos = _hexMath.AllLineNos(i).ToList();
                    for (int j=0;j<changeNos.Count();j++)
                        for (int k = 0; k < changeNos[j].Count();k++)
                            _hexSquares[changeNos[j][k]].IClick();
                }
        }
        private void ModeChange()
        {
            if (_squaresFaze == FAZE.firstChoose
                || _squaresFaze == FAZE.secondChoose)
                _squaresFaze = FAZE.range;
            else if (_squaresFaze == FAZE.range)
                _squaresFaze = FAZE.Line;
            else
                _squaresFaze = FAZE.firstChoose;
            AllCeler();
        }
        private void AllCeler()
        {
            for (int i = 0; i < _hexSquares.Count; i++)
                _hexSquares[i].IClickCancel();
        }
        private bool TargetNo(SquerMain sqMainBuff,out int result)//選択されているObjのNo取得
        {
            int i;
            var iMax = _hexSquares.Count();
            for (i = 0; i < iMax; i++)
            {
                if (_hexSquares[i] == sqMainBuff)
                {
                    result = i;
                    return true;
                }
            }
            result = 0;
            return false;
        }
        private bool IsControlFaze()//コントロール可能フェーズか
        {
            return (_squaresFaze == FAZE.firstChoose
                || _squaresFaze == FAZE.secondChoose
                || _squaresFaze == FAZE.range
                || _squaresFaze == FAZE.Line
                );
        }
    }
}