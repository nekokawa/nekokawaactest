﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.script.squares
{
    /// <summary>
    /// 配列一つで六角形を出すやつの計算
    /// </summary>
    class HexMath
    {
        public enum DIRECTION { up = 0, rightUpConer = 1, rightUp = 2, right = 3, rightDown = 4, rightDownConer = 5, down = 6, leftDownConer = 7, leftDown = 8, left = 9, leftUp = 10, leftUpConer = 11 }

        private int _xMax;
        private int _yMax;
        private int _max;

        private int _directionMax;

        public HexMath(int xMax, int yMax, int max)
        {
            _xMax = xMax;
            _yMax = yMax;
            _max = max;

            _directionMax = Enum.GetNames(typeof(DIRECTION)).Length;
        }

        //時計まわりで指定(hogehogeCornerのborderBuffはいづれ書き直したい)
        /// <summary>
        /// 指定した方向にあるマス番号を返します
        /// </summary>
        /// <param name="origin">現在地</param>
        /// <param name="direction">上を０以降は時計回りで左上の角を最終線とす</param>
        /// <returns></returns>
        public IEnumerable<int> SingleLineNos(int origin, int direction)
        {
            Func<int,bool> boderBuff;
            Func<int, int> nextFunc = LeftCorner;
            switch (direction%12) {
                case 0:
                    boderBuff = IsBorderYUp;
                    nextFunc = Up;
                    break;
                case 1:
                    boderBuff = new Func<int, bool>(x =>  IsBorderYUp(x) || IsBorderYDown(nextFunc(x),true) || IsBorderXright(x));
                    nextFunc = RightUpConer;
                break;
                case 2:
                    boderBuff = new Func<int, bool>(x => IsBorderYUp(x, true) || IsBorderXright(x));
                    nextFunc = RightUp;
                    break;
                case 3:
                    boderBuff = IsBorderXright;
                    nextFunc = RightCorner;
                    break;
                case 4:
                    boderBuff = new Func<int, bool>(x=> IsBorderYDown(x,true) || IsBorderXright(x));
                    nextFunc = RightDown;
                    break;
                case 5:
                    boderBuff = new Func<int, bool>(x => IsBorderYDown(x) || IsBorderYUp(nextFunc(x), true) || IsBorderXright(x));
                    nextFunc = RightDwonCorner;
                    break;
                case 6:
                    boderBuff = IsBorderYDown;
                    nextFunc = Down;
                    break;
                case 7:
                    boderBuff = new Func<int, bool>(x => IsBorderYDown(x) || IsBorderYUp(nextFunc(x), true) || IsBorderXLeft(x));
                    nextFunc = LeftDownCorner;
                    break;
                case 8:
                    boderBuff = new Func<int, bool>(x => IsBorderYDown(x,true) || IsBorderXLeft(x));
                    nextFunc = LeftDown;
                    break;
                case 9:
                    boderBuff = IsBorderXLeft;
                    nextFunc = LeftCorner;
                    break;
                case 10:
                    boderBuff = new Func<int, bool>(x => IsBorderYUp(x,true) || IsBorderXLeft(x));
                    nextFunc = LeftUP;
                    break;
                case 11:
                    boderBuff = new Func<int, bool>(x => IsBorderYUp(x) || IsBorderYDown(nextFunc(x), true) || IsBorderXLeft(x));
                    nextFunc = LeftUpCorner;
                    break;
                default:
                    yield break;
            }
            while (!boderBuff(origin) && LengsOverCheck(origin = nextFunc(origin)))
            {
                yield return origin;
            }

        }
        public IEnumerable<int> SingleLineNos(int origin, DIRECTION direction)
        {
            return SingleLineNos(origin, (int)direction);
        }

    //ーーーーーーーーーーーーーー境界線をすべて出力するだけのもの（不要になった）ーーーーーーーーーーーーーーーーーー
    /// <summary>
    /// 境界線上
    /// </summary>
    /// <param name="first">Trueの時奇数列</param>
    /// <returns></returns>
    public IEnumerable<int> BorderYUp(bool first)
        {
            var result = first ? 0 : _yMax;
            while (result < _max) {
                yield return result;
                result += _yMax * 2 - 1;
            }
        }
        public IEnumerable<int> BorderYUp()
        {
            var result = 0;
            var resultSecond = _yMax;
            while (result < _max)
            {
                yield return result;
                if (resultSecond < _max)
                    yield return resultSecond;
                result += _yMax * 2 - 1;
                resultSecond += _yMax * 2 - 1;
            }
        }
        public IEnumerable<int> BorderYDown(bool first)
        {
            var result = first ? _yMax - 1 : _yMax * 2 - 2;
            while (result < _max)
            {
                yield return result;
                result += _yMax * 2 - 1;
            }
        }
        public IEnumerable<int> BorderYDown()
        {
            var result = _yMax - 1;
            var resultSecond = _yMax * 2 - 2;
            while (result < _max)
            {
                yield return result;
                if (resultSecond < _max)
                    yield return resultSecond;
                result += _yMax * 2 - 1;
                resultSecond += _yMax * 2 - 1;
            }
        }
        public IEnumerable<int> BorderXLeft()
        {
            var result = 0;
            while (result < _yMax)
            {
                yield return result;
                result += 1;
            }
        }
        public IEnumerable<int> BorderXRight()
        {
            var buff = _xMax % 2 == 0 ? _yMax - 1 : _yMax;
            var result = _max - buff;
            while (result < _max)
            {
                yield return result;
                result += 1;
            }
        }
        //境界線判定(これは必須)
       private bool IsBorderYUp(int origin,bool first)
        {
            var result = first ? 0 : _yMax;
            return ((origin - result) % (_yMax * 2 - 1) == 0);
        }
        private bool IsBorderYUp(int origin)
        {
            return IsBorderYUp(origin, true) || IsBorderYUp(origin, false); 
        }
        private bool IsBorderYDown(int origin,bool first)
        {
            var result = first ? _yMax - 1 : _yMax * 2 - 2;
            return ((origin - result) % (_yMax * 2 - 1) == 0);
        }
        private bool IsBorderYDown(int origin)
        {
            return IsBorderYDown(origin, true) || IsBorderYDown(origin, false);
        }
        private bool IsBorderXLeft(int origin)
        {
            return origin < _yMax;
        }
        private bool IsBorderXright(int origin)
        {
            return _max - _yMax < origin;
        }

        //取得用の

        //範囲の障害物判定はA*で行うため、ここでは数字だけ取る（A*で処理を取るためなるべく軽く作りたい）
        /// <summary>
        /// 範囲ますを取得
        /// </summary>
        /// <param name="origin">指定位置</param>
        /// <param name="x">取得列</param>
        /// <returns></returns>
        //public IEnumerable<List<int>> RangeX(int origin, int x)
        //{
        //    for (int i = 0; i < x; i++)
        //    {
        //        var resultBuff = new List<int>();
        //        yield return
        //} }
   //重みづけがやりにくい方法なのでのちに修正予定
        public IEnumerable<int> RangeX(int origin,int rangeX)
        {
            var dictBuff = new Dictionary<int, int>();
            dictBuff[origin] = rangeX;
            return RangeXbuff(origin, rangeX,dictBuff).Where(x=>x!=origin).Distinct();
        }
        private IEnumerable<int> RangeXbuff(int origin, int rangeX, Dictionary<int, int> chexkList)
        {
            if (rangeX>0)
            {
                var buffList = RangeOne(origin);
                for (int j = 0; j < buffList.Count(); j++)
                {
                    var isNext = false;
                    if (!chexkList.ContainsKey(buffList[j]))
                    {
                        chexkList[buffList[j]] = rangeX;
                        isNext = true;
                    }
                    else
                    {
                        if( chexkList[buffList[j]] < rangeX)
                        {
                            chexkList[buffList[j]] = rangeX;
                            isNext = true;
                        }
                    }
                    if (isNext)
                    {
                        yield return buffList[j];
                        //                        checkList.Add(buffList[j]);
                        foreach (int intbuff in RangeXbuff(buffList[j], rangeX - 1, chexkList))
                            yield return intbuff;
                    }
                }
            }
        }
        //範囲計算系統
        public List<int> RangeOne(int origin)
        {
            var result = new List<int>();
            for (int i = 0; i < _directionMax; i+=2)
            {
                result.AddRange( SingleLineNos(origin, i).Take(1).ToList());
            }
            return result;
        }
        //private List<int> RangeMath(int origin,int x)
        //{
        //    var result = new List<int>();
        //    var buff = SingleLineNos(origin,DIRECTION.up).Skip(x-1).Take(1);


        //    yield return buff.Take();
        //}

        /// <summary>
        ///全ての方向への直線判定（中身は見ていない、すべてオリジンから順番に入っている）
        /// </summary>
        /// <param name="origin"></param>
        /// <returns>第一要素どの方向か、二要素それらのマス番号</returns>
        public IEnumerable<List<int>> AllLineNos(int origin )
        {
            for(int i=0;i< _directionMax; i++)
            {
                yield return SingleLineNos(origin, i).ToList();
            }
        }


        private int LeftUP(int origin)
        {
            return origin - _yMax;
        }
        private int LeftDown(int origin)
        {
            return origin - (_yMax - 1);
        }
        private int Up(int origin)
        {
            return origin - 1;
        }
        private int Down(int origin)
        {
            return origin + 1;
        }
        private int RightUp(int origin)
        {
            return origin + (_yMax - 1);
        }
        private int RightDown(int origin)
        {
            return origin + _yMax;
        }
        private int LeftCorner(int origin)
        {
            return origin - (_yMax * 2 - 1);
        }
        private int RightCorner(int origin)
        {
            return origin + (_yMax * 2 - 1);
        }
        private int LeftUpCorner(int origin)
        {
            return origin - (_yMax + 1);
        }
        private int LeftDownCorner(int origin)
        {
            return origin - (_yMax -2);
        }
        private int RightUpConer(int origin)
        {
            return origin + (_yMax - 2);
        }
        private int RightDwonCorner(int origin)
        {
            return origin + _yMax + 1;
        }






        //＃＃＃＃＃＃＃＃これ以降いらないかもだけどわかりやすいため残してる＃＃＃＃＃＃＃＃＃＃

        /// <summary>
        /// 特定の二点間の直線判定（需要はないかも）
        /// </summary>
        /// <param name="origin">原点</param>
        /// <param name="target">目標</param>
        /// <returns>チェックすべき配列番号を返却</returns>
        public IEnumerable<List<int>> LineNos(int origin, int target)
        {
            yield return LeftUpNos(origin, target).ToList();
            yield return LeftDownNos(origin, target).ToList(); ;
            yield return RightUpNos(origin, target).ToList(); ;
            yield return RightDownNos(origin, target).ToList(); ;
            yield return UpNos(origin, target).ToList(); ;
            yield return DownNos(origin, target).ToList(); ;
        }

        public IEnumerable<int> LeftUpNos(int origin, int target)
        {
            if (LeftUpNos(origin).All(x => x == target))
            {
                while (origin == target)
                {
                    origin = LeftUP(origin);
                    yield return origin;
                }
            }
        }
        public IEnumerable<int> LeftUpNos(int origin)
        {
            return SingleLineNos(origin, DIRECTION.leftUp);
            //var endBuff = Enumerable.Concat(BorderYUp(true), BorderXLeft()).ToList();//実体化
            //while (endBuff.All(x => x != origin) && LengsOverCheck(origin))
            //{
            //    origin = LeftUP(origin);
            //    yield return origin;
            //}
        }
        public IEnumerable<int> LeftDownNos(int origin, int target)
        {
            if (LeftDownNos(origin).All(x => x == target))
            {
                while (origin == target)
                {
                    origin = LeftDown(origin);
                    yield return origin;
                }
            }
        }
        public IEnumerable<int> LeftDownNos(int origin)
        {
            return SingleLineNos(origin, DIRECTION.leftDown);
        }
        public IEnumerable<int> UpNos(int origin, int target)
        {
            if (UpNos(origin).All(x => x == target))
            {
                while (origin == target)
                {
                    origin = Up(origin);
                    yield return origin;
                }
            }
        }
        public IEnumerable<int> UpNos(int origin)
        {
            return SingleLineNos(origin, DIRECTION.up);
        }
        public IEnumerable<int> DownNos(int origin, int target)
        {
            if (DownNos(origin).All(x => x == target))
            {
                while (origin == target)
                {
                    origin = Down(origin);
                    yield return origin;
                }
            }
        }
        public IEnumerable<int> DownNos(int origin)
        {
            return SingleLineNos(origin, DIRECTION.down);
        }
        public IEnumerable<int> RightUpNos(int origin, int target)
        {
            if (RightUpNos(origin).All(x => x == target))
            {
                while (origin == target)
                {
                    origin = RightUp(origin);
                    yield return origin;
                }
            }
        }
        public IEnumerable<int> RightUpNos(int origin)
        {
            return SingleLineNos(origin, DIRECTION.rightUp);
        }
        public IEnumerable<int> RightDownNos(int origin, int target)
        {
            if (RightDownNos(origin).All(x => x == target))
            {
                while (origin == target)
                {
                    origin = RightDown(origin);
                    yield return origin;
                }
            }
        }
        public IEnumerable<int> RightDownNos(int origin)
        {
            return SingleLineNos(origin, DIRECTION.rightDown);
        }
        private bool LengsOverCheck(int origin)
        {
            return 0 <= origin && origin < _max;
        }
        //角の直線判定
        public IEnumerable<int> LeftCornerNos(int origin)
        {
            return SingleLineNos(origin, DIRECTION.left);
        }
        public IEnumerable<int> RightCornerNos(int origin)
        {
            return SingleLineNos(origin, DIRECTION.right);
        }
        //errer招致状態
        public IEnumerable<int> LeftUpCornerNos(int origin)
        {
            return SingleLineNos(origin, DIRECTION.leftUp);
        }
        public IEnumerable<int> LeftDownCornerNos(int origin)
        {
            return SingleLineNos(origin, DIRECTION.leftDown);
        }



    }
}
