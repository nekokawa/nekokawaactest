﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.script.squares
{
    class MakeHexa
    {
        readonly float SPACE_ADD = 1.2f;
        //gameobjectでなくマス情報を返ことになる？
        /// <summary>
        /// 配列一つのタイプ。端っこが面倒(これで結構な問題になっている)
        /// </summary>
        /// <param name="makesX">xマスの最大数</param>
        /// <param name="makesY">yマスの最大数</param>
        /// <returns>作られたゲームオブジェクトたち</returns>
        /// /// <param name="hexGameObject"></param>
        /// <returns></returns>
        public IEnumerable<SquerMain> MakeHexaSquears(int makesX, int makesY, GameObject hexGameObject, Transform parentTF)
        {
            if (hexGameObject.GetComponent(typeof(SquerMain)))
            {
                var space = hexGameObject.transform.localScale * SPACE_ADD;
                var namebuff = 0;
                for (int i = 0; i < makesX; i++)
                {
                    var boolbuff = (i + 2) % 2 == 0;
                    var jMax = (boolbuff ? makesY : makesY - 1);
                    var harfAdd = boolbuff ? 0 : space.y / 2;
                    for (int j = 0; j < jMax; j++)
                    {
                        var hexObjBuff = GameObject.Instantiate(hexGameObject, parentTF, false);
                        hexObjBuff.transform.position = new Vector3(i * space.x, 0, j * space.z + harfAdd);
                        hexObjBuff.name = (namebuff).ToString();
                        namebuff++;
                        yield return hexObjBuff.GetComponent<SquerMain>();
                    }
                }
            }
        }
        /// <summary>
        /// 配列の所持数が1.5倍くらいになることと、中身のないものができるが計算が楽で見た目もわかりやすい(全てに○○するってのが難しい)
        /// </summary>
        /// <param name="makesX"></param>
        /// <param name="makesY"></param>
        /// <param name="hexGameObject"></param>
        /// <param name="parentTF"></param>
        /// <param name="returnSquers"></param>
        /// <returns></returns>
        public bool MakeHexaSquears(int makesX, int makesY, GameObject hexGameObject, Transform parentTF, out SquerMain[][] returnSquers)
        {
            makesY = makesY * 2;
            var result = false;
            returnSquers = new SquerMain[makesX][];
            for (int i = 0; i < makesX; i++)
                returnSquers[i] = new SquerMain[makesY];
            if (hexGameObject.GetComponent(typeof(SquerMain)))
            {
                var space = hexGameObject.transform.localScale * SPACE_ADD;
                var namebuff = 0;
                for (int i = 0; i < makesX; i++)
                {
                    var boolbuff = (i + 2) % 2 == 0;
                    var jStart = boolbuff ? 0 : 1;
                    for (int j = jStart; j < makesY; j += 2)
                    {
                        var hexObjBuff = GameObject.Instantiate(hexGameObject, parentTF, false);
                        hexObjBuff.transform.position = new Vector3(i * space.x, 0, j * space.z / 2);
                        hexObjBuff.name = (namebuff).ToString();
                        namebuff++;
                        returnSquers[i][j] = hexObjBuff.GetComponent<SquerMain>();
                    }
                }
                result = true;
            }
            return result;
        }
    }
}
